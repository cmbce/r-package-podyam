# Modèle de Cynthia #
Faire attention aux PATH de sauvegarde dans RoutineMoSi.R et aux PATH dans dataextraction.R (fin de fichier)

Ensuite, pour faire les graphs de l'article de Cynthia
```R
source('~/Documents/DEV/datamusclr/RoutineMoSi.R', chdir = TRUE) # fait toutes les simulations et enregistre les résultats en .rda
source("~/Documents/DEV/datamusclr/r-package-podyam/podyam/inst/for_cynthia/Scripts/dataextraction.R") # créé les figures à partir des .rda
```


# Dependencies #
* lubridate
* data.table
* stringr
* DataManagement
* Iterators


# Install #
Clone this wherever you want `git clone git@bitbucket.org:cmbce/r-package-podyam.git`
Then in R
```R
setwd('PATH_TO/r-package-podyam/podyam/')
document()
test()
install()
```

# Get started #

* Copy the folder `tests/testthat` for examples
* The `scenarii` directory contains some test that can be used as examples

## The scenario file
This file contains the location of the other Rscripts needed by poydam along with some information about the current run.
**Note that the 3 variables `OUT_PATH`, `RUN_ID` will be used to create the path for all outfiles, and `MODEL_ID` for creating the filenames relatively to the location of your scenario**

## The parameters file
This is where the model is built and where you will define the variables to fit.

## Run a basic simulation
The scenario file is where you define all informations about your simulation

* The scenario is located in `tests/testthat/scenarii/basic_model_noOptim.R`
* * Note that the last line of this file will delete all create files
* The parameters are located in `tests/testthat/scenarii/simple_mosaic_phoma_basic_model_noOptim.R`
* * The extraction module is deactivated here, you can reactivate it by uncommenting its line in PROCESSES line 98

```R
source("tests/testthat/scenarii/basic_model_noOptim.R", chdir = TRUE)
```

## Run an optimization
```R
source("tests/testthat/scenarii/basic_model_optim.R", chdir = TRUE)
```
Note: Have a look at how you can define your bounds in `test/testthat/parameters/tagged_param_only.R`

# DEV #
While creating modules or functions for existing ones it is possible to update the documentation automatically by using a Python3 script.
See the help and examples for more information on this script.

```bash
cd podyam/
python3 inst/hooks/help_generator/Functions_automatic_Integration_roxygen_doc/src/fun_auto_integration_roxygen.py -i R/
```

## Create a new module
1. Your module should be in one Rscript
2. The first lines must be formated as follow in order to be found by the python script (for documentation)

```R
######################
# Module description #
######################
#' @title a nice title
#' @name module_name
#' @description desc
#'
#' The main function is my_main()
#' @usage NULL
######################
# END OF MODULE DESC #
######################
```

3. The roxygen documentation of the module itself must contain two tags to be recognized by the python script (for documentation). Currently the auto-generated doc is inserted between <TAG2INCLUDE>

```R
#' <TAG2INCLUDE>
#' The doc will be here
#' <TAG2INCLUDE>
```

## Create a new function for a module
The different function a module can use are automatically added to the doc via external script, thus there is some rules to follow:

1. The functions must be in the same file as the module
2. Use roxygen2 structure
3. **Define explicitly** the tags `@name`, `@title` and fill them
4. Only the comment blocks with the `@export` tag will be added to the module doc
5. No empty lines or non-roxygen comment symbol in the middle of a comment block

_TO-DO: récup script listing module autre pc_