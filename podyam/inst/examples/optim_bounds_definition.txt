\dontrun{
Example of prior definition in the param file:

fastDisp=list(
        Lepto.spore=list(
                condition="",
                condVisu="FALSE",
                distFunction="Gaussiankernel",
                parametres=list(
                    mean=0,
                    fit_sd=1000,
                ),
                bounds=list(
                        bound_sd=c(0,10000)
                )
        )
)
}
