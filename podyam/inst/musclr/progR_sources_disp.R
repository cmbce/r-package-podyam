library("Rcpp")
library("testthat")
####################################################fonctions au choix pour le calcul de gradient dans dispersion isotrope a longue distance, ou l envie de se disperser, ou la mortalite postdeplacement
puissanceneg<-function(d, a, b) {
  return(a/(d^b))
}

ExponentialKernel<-function(d, y0=1, r) 
{
  return(y0 * exp(-d/r))
}
expodecroi<-function(d, y0=1, r) 
{
  return(y0 * exp(-r*d))
}

puissanced<-function(d, a)
{
  return(a^d)
}

expoDEPCI<-function(d, m0)
{
#d apres these de Julien Papaix, sauf que signe moins ajoute dans l exponentielle (coquille dans la these)
  return(2*pi/(m0^2)*exp(-2*pi/m0*d))
}
#exple= plot(expoDEPCI(0:200, 150))

rapepollendispersal<-function(d, lim1=1.5, lim2=50, a0=0.340, a1= -0.405, a2=0.128, b0=0.03985, b1=3.12, b2=3.80, rr=50, gamma= -2.29)
{
#traduction de fonction 1 de califlopp
  #on cree vecteur de resultat contenant que des 0
  rt<-numeric(length(d))
  # en dessous de certaine distance, on utilise une fonction quadratique
  cas1<- (d <= lim1)
  rt[cas1] <- a0 + a1 * d[cas1] + a2 * d[cas1]^2
  #au dessus d une certaine distance on utilise une autre fonction
  cas2<- (d > lim2)
  K <- b0 / (1.0 + d[cas2]^b1 / b2)  / (1.0 + rr)^gamma
  rt[cas2] <- K *  (1.0 + d[cas2])^gamma
  #entre les deux on utilise encore une autre
  rt[!cas1 & !cas2] <- b0 / (1.0 + d[!cas1 & !cas2]^b1 / b2)
  return(rt)
}
#exple= plot(rapepollendispersal(0:20), type="l")

rapeseeddispersal<-function(d, grainesB= 1.38930, grainesC= 2.08686, dlim=21)
{
#traduction de fonction 2 de califlopp
  #on cree vecteur de resultat contenant que des 0
  rt<-numeric(length(d))
  # en dessous de la distance maximale parcourue :
  rt[d<=dlim] <-(grainesB * grainesC) * d[d<=dlim]^(grainesC - 2) *
    exp (-grainesB * d[d<=dlim]^grainesC) / (2.0 * pi)
  return(  rt )
}
#exple= plot(x<-seq(0,50, length.out=100), y=rapeseeddispersal(x), col="red")
#plot(x<-seq(0,50, length.out=100), y=rapepollendispersal(x), type="l", col="black")
#lines(x<-seq(0,50, length.out=100), y=rapeseeddispersal(x), col="red")
#lines(x<-seq(0,50, length.out=100), expoDEPCI(x, 30), col="green")

constant<-function(d, value) {
  return(value)
}

Gaussiankernel<-function(d, mean, sd)
{
	return(dnorm(x=d, mean=mean, sd=sd))
}

Gammakernel<-function(d, shape, rate)
{
  return(dgamma(x=d, shape=shape, rate=rate))
}
# # visual test
# xs <- 0:100
# plot(xs,Gaussiankernel(xs,50,10))


############################################################# dispersion isotrope (on ne fait tout ca que si au moins un organisme a dispiso

 #on cherche les e.s.c qui ont dispersion continue
toto<-lapply(PARAMETRES, "[[", "dispiso")
names(toto)<-NULL
paramscontinus <- unlist(toto,recursive=FALSE)
if (length(paramscontinus)>0) {

#library(abind)
  #################calculs intermÃ©diaires qu on ne fait tourner qu une seule fois au dÃ©but du programme
  #tableau pour la dispersion spatialement explicite (de niveau sup Ã  niveau sup): 
  #calcule une bonne fois pour toutes des distances sÃ©parant les pixels 
  # nb espace periodique, on prend uniquement la plus petite distance entre deux points
  DISTANCES<-data.frame(
		from=rep(rownames(ESPACE[[paste("lev", NBNIV, sep="")]]), each=(TP^2)), 
		to=rep(rownames(ESPACE[[paste("lev", NBNIV, sep="")]]), (TP^2))
  )
  DISTANCES<-DISTANCES[DISTANCES$from!=DISTANCES$to,]
  splitfrom<-strsplit(DISTANCES$from, split="_")
  splitto<-strsplit(DISTANCES$to, split="_")
  xfrom<-as.numeric(sapply(splitfrom, "[[",1))
  xto<-as.numeric(sapply(splitto, "[[",1))
  yfrom<-as.numeric(sapply(splitfrom, "[[",2))
  yto<-as.numeric(sapply(splitto, "[[",2))
  #on corrige dx et dy pour avoir PBC
  dx<-xto-xfrom
  dx<-dx-round(dx/TP)*TP
  dy<-yto-yfrom
  dy<-dy-round(dy/TP)*TP
  DISTANCES$d<-sqrt(dx^2+dy^2)*TC^(NBNIV-1)*GRAIN
  rm(splitfrom, splitto, xfrom, yfrom, xto, yto, dx, dy)


  ######### ajout du niveau NBNIV+1 pour dispersion dans le paysage

  toto<-ESPACE[paste("lev", NBNIV, sep="")]
  names(toto)<-paste("lev", NBNIV+1, sep="")
  toto[[1]][,"level"]<-NBNIV+1
  toto[[1]][,paste("pixfather", NBNIV+1, sep="")]<-NA
  ESPACE<-c(ESPACE, toto)
  rm(toto)


############fonctions pour dispersion continue
#fonction qui dÃ©multiplie DISTANCES (matrice contenant les distances entre les pixels de niveau le plus haut)
#en ajoutant une dimension supÃ©rieure:  bioag.stade.cas
#et qui calcule la proba d atterrir dans chacune des cases 
#ATTENTION: j ai pas fait vraie intÃ©gration: on rÃ©partit juste proportionnellement Ã  
# valeurs des gradients pour les diffÃ©rentes distances (pas de pertes de spores sur les cotÃ©s))
#renvoie une matrice de meme nb de lignes que DISTANCES et nb de colonnes= nb de es.sta.cas (cad longueur de listeparamesc)
#DISPERSE est calculee une bonne fois pour toutes au debut de chaque simulation a l aide de cette fonction
demultiplieDISTANCES<-function(listeparamesc)
{  
   resultat<-DISTANCES
   uncas<-function(esc)
   { 
     probas<-do.call(listeparamesc[[esc]]$fonction, 
                           c(list(d=DISTANCES$d), listeparamesc[[esc]]$parametres ))
     total<- rowsum(probas, group=DISTANCES$from, reorder=TRUE)
     rownames(total)<-sort(unique(DISTANCES$from))
     probas<-array(probas/total[DISTANCES$from,], dim=c(length(probas),1), dimnames=list(NULL, esc))
     resultat<<-cbind(resultat,probas)
     return()
   }
   lapply(X=names(listeparamesc), FUN=uncas)
   return(resultat)
}
#exple: toto<-demultiplieDISTANCES(PARAMETRES$septo$dispiso)


#fonction qui crÃ©e, le tableau des vecteurs des probas de monter a chaque niveau sup pour chaque esp.sta.cas  
###ATTENTION: pour l instant elle est fausse (on fait juste intÃ©gration trapezoidale du gradient), juste pour avoir qqchose
###il faudra en particulier faire attention a prendre en compt ce qui retombe dans le meme pixel ap partir des niveaux sups
# renvoie une matrice de nb de lignes = es.sta.cas (cad longueur de listeparamesc), nb colonnes= NBNIV+1,
# REPARTITION est calculee une bonne fois pour toutes au debut de chaque simulation a l aide de cette fonction
probamonte<-function(listeparamesc)
{
  uncas<-function(esc)
  {
    e<-unlist(lapply(strsplit(esc, ".", fixed=TRUE), "[[",1))
    nivbase<-as.numeric(PARAMETRES[[e]]$niv)
    distances<-c(0,GRAIN*TC^((1:NBNIV)-1),TP*GRAIN*TC^(NBNIV-1)) #on rajoute taille de tout le paysage pour borne sup du niveau NBNIV+1
    y<-do.call(listeparamesc[[esc]]$fonction, args=c(listeparamesc[[esc]]$parametres, list(d=distances))) 
    area<-numeric(length(distances)-1)
    for (i in 1:(length(distances)-1)) area[i]<-(y[i]+y[i+1])*(distances[i+1]-distances[i])/2
    #on additionne ceux qui sont inferieurs a nivbase
    if (nivbase>1) { area[nivbase]<-sum(area[1:nivbase])
                     area[1:(nivbase-1)]<-0 }
    tot<-sum(area)
    return(area/tot)
  }
  resultat<-sapply(X=names(listeparamesc), FUN=uncas, simplify=TRUE)
  return(t(resultat))
}
#exple: toto<-probamonte(PARAMETRES$septo$dispiso) toto<-probamonte(paramscontinus)

####initialisation du tableau de rÃ©partition des propagules en classes de distances
  #on applique demultiplieDISTANCES et probamonte uniquement sur les bioag qui ont dispersion continue

	DISPERSE<-demultiplieDISTANCES(listeparamesc= paramscontinus)
	REPARTITION<-probamonte(listeparamesc= paramscontinus)
	colnames(REPARTITION)<-paste("lev", 1:dim(REPARTITION)[2], sep="")


#fonction qui calcule le nombre de propagules d un esp.sta.cas qui sont montÃ©es par niveau
#renvoie un liste Ã  NBNIV-niv elements (niveaux niv Ã  NBNIV+1) de matrices a 1 colonne et n lignes (n= nb de pixels ou condition est true dans au moins un pixel inferieur)
###remarque: on fonctionne en sommes (si represents>1, le chiffre dans le pixel est deja la somme des voisins
faitmonter<-function(esc) 
{ 
	e<-unlist(lapply(strsplit(esc, split=".", fixed=TRUE), "[[", 1))
	s<-unlist(lapply(strsplit(esc, split=".", fixed=TRUE), "[[", 2))
        condition<-eval(PARAMETRES[[c(e, "dispiso", esc)]]$parsed_condition) & POPULATIONS[[e]][,s]>0
        niv<-as.numeric(PARAMETRES[[e]]$niv)
        niveaux<-niv:(NBNIV+1) ; names(niveaux)<-paste("lev", niv:(NBNIV+1), sep="")

	propagules<-  ESPACE[[paste("lev", niv, sep="")]][condition,"represents"] * POPULATIONS[[e]][condition,s, drop=FALSE]
 	matrice<-propagules[propagules[,1]>0,,drop=FALSE] %*% REPARTITION[esc,paste("lev", niv:(NBNIV+1), sep=""),drop=FALSE]      #i.e. matrice= [pixelssources, levelarrivee]
	resultat<-lapply(niveaux, function(n) if (n==niv) return(matrice[,paste("lev", n, sep=""),drop=FALSE]) else return(rowsum(matrice[,paste("lev", n, sep=""),drop=FALSE], group=ESPACE[[paste("lev", niv, sep="")]][condition,paste("pixfather",n, sep="")], reorder=FALSE)))
	return(resultat)
}

faitspatialexpl<-function(esc, propanivmax)
{ 
	sanstrous<-matrix(
		0, ncol=1, nrow=dim(ESPACE[[paste("lev", NBNIV+1, sep="")]])[1], 
		dimnames=list(rownames(ESPACE[[paste("lev", NBNIV+1, sep="")]]), paste("lev", NBNIV+1, sep=""))
	)
	sanstrous[rownames(propanivmax),]<-propanivmax
	arrivees<-rowsum(sanstrous[DISPERSE$from,,drop=FALSE] * DISPERSE[,esc], group=DISPERSE$to, reorder=FALSE)
	return(arrivees[rownames(ESPACE[[paste("lev", NBNIV, sep="")]]),, drop=FALSE])        
}

#fonction pour sommer terme a terme des matrices placees dans une liste
sommematrices<-function(...)
{
	liste<-list(...)
	niv<-colnames(liste[[1]])
	x <- matrix(0, ncol=1, nrow=dim(ESPACE[[niv]])[1], dimnames=list(rownames(ESPACE[[niv]])))
	lapply(seq_along(liste), function(i){
		x[rownames(liste[[i]]),] <<- x[rownames(liste[[i]]),] + liste[[i]]
		return(invisible())
	})
	return(x)
}
#fonction pour sommer terme a terme les matrices de chaque niveau d une liste de cas
sommecas<-function(...) do.call(function(...) {mapply(FUN=sommematrices, ..., SIMPLIFY=FALSE )}, args=...)


###remarque: on fonctionne en sommes (si represents>1, le chiffre dans le pixel est deja la somme des voisins
#propagules = list a NBNIV+1 niveaux de matrices a une colonne
faitredescendre<-function(e.s, propagules) 
{  
	e<-unlist(lapply(strsplit(e.s, split=".", fixed=TRUE), "[[", 1))
	s<-unlist(lapply(strsplit(e.s, split=".", fixed=TRUE), "[[", 2))
        niv<-as.numeric(PARAMETRES[[e]]$niv)
        #verif<-sum(sapply(propagules, sum))
        niveaux<- (NBNIV):niv ; names(niveaux)<-paste("lev", (NBNIV):niv, sep="")
	for (n in niveaux) { #n= niveau d arrivee (niveau inf)
		IDsperes<-ESPACE[[paste("lev", n, sep="")]][,paste("pixfather", n+1, sep="")]
		facteurdivision<-ifelse(n==NBNIV | ESPACE[[paste("lev", n+1, sep="")]][IDsperes, "homogene"], 1,TC^2)
		result<-matrix(0, ncol=1, nrow=dim(ESPACE[[paste("lev", n, sep="")]])[1], dimnames=list(rownames(ESPACE[[paste("lev", n, sep="")]])))
		result[rownames(propagules[[paste("lev", n, sep="")]]),]<-propagules[[paste("lev", n, sep="")]]
		result<-result+propagules[[paste("lev", n+1, sep="")]][ IDsperes,,drop=FALSE ]/facteurdivision
		propagules[[paste("lev", n, sep="")]]<-result
	}
	colnames(result)<-e.s
        #verif2<-sum(result)
        #diff<- verif - verif2
        #if (abs(diff) > 0.1) warning(paste(diff, "propagules of", e.s, "were lost in the dispersal process"))
	return(result)
}


#fait dispersion isotrope pour un stade (toutes conditions) 
dispersioncontinue<-function(e.s)
{
  splits<-strsplit(e.s, split=".", fixed=TRUE)
  e<-unlist(lapply(splits, "[[", 1))
  s<-unlist(lapply(splits, "[[", 2))
  listesc<-names(PARAMETRES[[e]]$dispiso)
  listesc<-listesc[grepl(paste(paste(e,s,sep="."),".", sep=""), listesc, fixed=TRUE)]
  yenatil<-unlist(lapply(listesc, function(x) {
	condition<-eval(PARAMETRES[[e]]$dispiso[[x]]$parsed_condition)
	return(sum(POPULATIONS[[e]][condition,s])>0)
  }))
  if (sum(yenatil)>0) {
	#on fait monter et dispersion plus haut niveau
	montees<-lapply(listesc[yenatil], function(esc) {
		propagules<-faitmonter(esc) 
		propagules[[paste("lev", NBNIV+1, sep="")]]<-faitspatialexpl(esc, propanivmax=propagules[[paste("lev", NBNIV+1, sep="")]]) 
		return(propagules)
  	})
	#on somme les propagules venant des differents cas
	touscas<-sommecas(montees)
	#on redistribue vers le niveau de la pop
	arrivees<-faitredescendre(e.s, touscas)
 	niv<-PARAMETRES[[e]]$niv
 	POPULATIONS[[e]][,s] <<- arrivees / ESPACE[[paste("lev", niv, sep="")]][,"represents"]
  }
  return()
}

#NB on peut pas utiliser exactement meme schema que autres fonctions car pour dispersion, il faut sommer toutes les propagules qui arrivent dans un pixel, qqsoit leur condition de depart
#en plus, pas de fonction ni de parametres car DISPERSE et REPARTITION sont calculees au debut de la simulation ou quand on modifie les parametres en cours de simu
dispiso<-function() 
{
  toto<-PARAMETRES
  names(toto)<-NULL
  noms<-unlist(lapply(toto, function(x) {
	names(x$dispiso)
	}), recursive=FALSE)
  if (OPTIONSIMU$printwhat>2) print(paste("isotropic dispersal of", paste(noms, collapse=" ")))
  if(length(noms)==0){
      if (OPTIONSIMU$printwhat>2) print("no organism has isotropic dispersal")
      return()
  }else{
      splits<-strsplit(noms, split=".", fixed=TRUE)
      especes<-unlist(lapply(splits,"[[", 1))
      stades<-unlist(lapply(splits,"[[", 2))
      #on garde que les esp.sta car dans dispersioncontinue il faudra sommer tous les migrants qui viennent des differents cas
      noms<-unique(paste(especes, stades, sep="."))
      # temps<-system.time(lapply(noms, dispersioncontinue))[2]
      # return(temps)
      lapply(noms, dispersioncontinue)
  }
}

} #fin de if (length(paramscontinus)>0) 




############################################################# orientee (on ne fait tout ca que si au moins un organisme a dispori)
 #on cherche les e.s.c qui ont dispersion orientee
toto<-lapply(PARAMETRES, "[[", "dispori")
names(toto)<-NULL
paramsorientes <- unlist(toto,recursive=FALSE)
if (length(paramsorientes)>0) {

##########fonctions pour dispersion orientee (existence d une fenetre de perception)
 #on cherche les e.s.c qui ont dispersion orientee

quelring<-function(rowdep, coldep, rowarr, colarr)
{
#trouve a quel anneau (anneaux concentriques de rayon 1,2,3, etc. pixels) autour d un pixel de depart appartient un pixel d arrivee 
    d<-sqrt((rowarr-rowdep)^2 + (colarr-coldep)^2)
   return(floor(d)) 
}

trouvevoisins<-function(nbrings)
{
	row<-rep(-nbrings: nbrings, times=2* nbrings+1)
	col<-rep(-nbrings: nbrings, each=2* nbrings+1)
	toto<-quelring(0,0, row, col)
	toto[toto> nbrings]<-NA
	titi<-data.frame(row, col, ring=toto)
	titi<-titi[!(is.na(titi$ring) ),]
	titi<-titi[order(titi$ring),]
	titi$num<-unlist( aggregate(titi$ring, by=titi[,"ring", drop=FALSE], function(x) list(1:(length(x))))$x )
	rownames(titi)<-paste("v", titi$ring, titi$num, sep=".")
	return(titi[,c("ring", "row", "col")])
}
#exple: toto<-trouvevoisins(6); plot(-6:7, -6:7, type="n"); rect(xleft=toto$col, ybottom=toto$row, xright=toto$col+1, ytop=toto$row+1, col=toto$ring)

calculeENVIES<-function(window, precisionring=2, fonction, parametres)
{
#renvoie un data.frame avec le potentiel de dispersion en fonction de la distance en nbrow et nbcol (pixels au niveau de dispersion i.e. au niveau 
#qui permet d avoir plus de precisionring cercles concentriques autour du point de depart dans la fenetre de perception
  #on choisit niveau de dispersion pour etre le plus grand niveau qui permet d avoir plus de precisionring anneaux dans window 
  #(attention:fait aussi dans fonction vol, si modifs ici, changer la-bas)
  nivdisp<-which.max(which(precisionring*GRAIN*TC^(1:NBNIV-1) < window, arr.ind=TRUE))
  nbrings<- ceiling(window / (GRAIN*TC^(nivdisp-1)))
  #on calcule les distances entre le pixel central et ses voisins
  rings<-trouvevoisins(nbrings)
  rings$d<-rings$ring*GRAIN*TC^(nivdisp-1)
  #on calcule envie de se disperser dans chaque voisins
  rings$pot<-do.call(fonction, c(list(d=rings$ring*GRAIN*TC^(nivdisp-1)), parametres))
  #on renormalise a 1 (pas necessaire ici car on renormalise apres avoir multiplie par attractivite)
  #rings$pot<-rings$pot/sum(rings$pot)
  return(rings[ceiling(rings$d)<=window,c("d", "row", "col", "pot")])
}
#exple: toto<-calculeENVIES(precisionring=2, window=300, fonction="expodecroi", parametress=list(desp=50)); plot(-6:7, -6:7, type="n"); rect(xleft=toto$col, ybottom=toto$row, xright=toto$col+1, ytop=toto$row+1, col=gray((max(toto$pot)-toto$pot)*40))
#NB: il serait possible de calculer ENVIES au debut de simulation une bonne fois pour toutes, mais il faudrait reflechir a structure en fonction des especes et conditions

#on calcule ENVIES de se disperser autour d'un point de dÃ©part notÃ© 0 une bonne fois pour toutes
SmartErrorsOnlapply <- function(listObjects,functionType,parametersType,errorMessage=""){
    out <- list()
    for(iObject in 1:length(listObjects)){
        nameObject <- names(listObjects)[[iObject]]
        lp <- listObjects[[nameObject]]
        out[[nameObject]] <- try(do.call(lp[[functionType]],lp[[parametersType]]))
        if(class(out[[nameObject]])== "try-error"){
            errorMessage <- paste(errorMessage,
                                  "Error in params of",lp[[functionType]],"for",nameObject,"\n")
            cat(errorMessage)
        }
    }
    return(out)
}
ENVIES <-SmartErrorsOnlapply(paramsorientes,"fonctionenvie","parametresenvie")

# # Old, not helpful way
# ENVIESbis <-lapply(paramsorientes, function(lp) do.call(lp$fonctionenvie, lp$parametresenvie))

# can be visualized with: 
# library("spatDataManagement")
# with(ENVIES[[1]],plot(a$row,a$col,col=XToColors(a$pot),pch=19))

vol<-function(esc)
{
	e<-unlist(lapply(strsplit(esc, split=".", fixed=TRUE), "[[", 1))
	s<-unlist(lapply(strsplit(esc, split=".", fixed=TRUE), "[[", 2))
    condition<-eval(PARAMETRES[[c(e, "dispori", esc)]]$parsed_condition)
	nivpop<-as.numeric(PARAMETRES[[e]]$niv)

  	#on choisit niveau de dispersion pour etre le plus grand niveau qui permet d avoir plus de precisionring anneaux dans window
	# (attention: deja fait a l interieur de calculeENVIES, si modifs ici, changer la-bas aussi)
	nivdisp<-which.max(which(PARAMETRES[[e]]$dispori[[esc]]$parametresenvie$precisionring*GRAIN*TC^(1:NBNIV-1) < PARAMETRES[[e]]$dispori[[esc]]$parametresenvie$window, arr.ind=TRUE))

#on calcule l attractivite des pixels au niveau de la pop (forcage + application de la fonction fonctionattract) et au niveau de la dispersion (changelevel + desimplification)
	paramattractpop<-lapply(PARAMETRES[[e]]$dispori[[esc]]$parametresattract, forcage, niv=nivpop)
	attractpop<-data.frame(attractivity=do.call(PARAMETRES[[e]]$dispori[[esc]]$fonctionattract, paramattractpop))
	rownames(attractpop)<-rownames(ESPACE[[paste("lev", nivpop, sep="")]])

	# et au niveau ou il y a dispersion comme la moyenne de l attractivite des pixels ou la pop est definie 
	attractdisp<-changelevel(df= attractpop, nivdep=nivpop, nivarr=nivdisp, method="uncountable")
	#on desimplifie
	attractdisp<-attractdisp[ESPACECOMPL[[paste("compllev", nivdisp, sep="")]][,"reprby"],  ,drop=FALSE] 
	rownames(attractdisp)<-rownames(ESPACECOMPL[[paste("compllev", nivdisp, sep="")]])

#on calcule les parametres de la fonction de calcul du taux de survie postdeplacement des pixels au niveau de la pop (forcage) et au niveau de la dispersion (changelevel + desimplification)
#attention: contrairement a attract, ici on fait moyenne des parametres, pas moyenne du taux de survie lui-meme, car fonctionsurvie est appliquee dans fonction distribueunecase
	paramssurviepop<-as.data.frame(lapply(PARAMETRES[[e]]$dispori[[esc]]$parametressurvie, forcage, niv=nivpop))
	rownames(paramssurviepop)<-rownames(ESPACE[[paste("lev", nivpop, sep="")]])
	paramssurviedisp<-changelevel(df=paramssurviepop, nivdep=nivpop, nivarr=nivdisp, method="uncountable")
	#on desimplifie
	paramssurviedisp <-paramssurviedisp[ESPACECOMPL[[paste("compllev", nivdisp, sep="")]][,"reprby"],  ,drop=FALSE] 
	rownames(paramssurviedisp)<-rownames(ESPACECOMPL[[paste("compllev", nivdisp, sep="")]])

#on calcule quantites d individus a disperser
	#on somme le individus a disperser au niveau de la dispersion
	adisperser<-POPULATIONS[[e]][,s, drop=FALSE]
	adisperser[!condition,]<-0
	somme<-changelevel(df=adisperser, nivdep=nivpop, nivarr=nivdisp, method="countable")

	#et on desimplifie
	depart<-matrix(c(somme[ ESPACECOMPL[[paste("compllev", nivdisp, sep="")]][,"reprby"] ,],
		ESPACECOMPL[[paste("compllev", nivdisp, sep="")]]$row,
		ESPACECOMPL[[paste("compllev", nivdisp, sep="")]]$col), ncol=3)
	dimnames(depart)<-list(rownames(ESPACECOMPL[[paste("compllev", nivdisp, sep="")]]), c("qte", "coordx", "coordy"))

	#on garde que ceux qui ont des individus a disperser
	depart<-depart[depart[,"qte"]>0,,drop=FALSE]

#on va sommer les individus qui arrivent sur chaque case d arrivee a partir de chaque case de depart, au niveau nivdisp
	#prepare vecteur d arrivees
	arrivdisp<-numeric(dim(ESPACECOMPL[[paste("compllev", nivdisp, sep="")]])[1])
	names(arrivdisp)<-rownames(ESPACECOMPL[[paste("compllev", nivdisp, sep="")]])

	nbcases<-TP*TC^(NBNIV-nivdisp)
	nRows <- nbcases
	nCols <- nbcases
	# TODO: these should be directly correcly ordered
	CoordNamesToOrdering <- function(db,nRows){
	    cellRef <- simplify2array(strsplit(rownames(db),"_"))
	    rows <- as.numeric(cellRef[1,])
	    cols <- as.numeric(cellRef[2,])
	    out <- db[rows+(cols-1)*nRows,,drop=FALSE];
	    return(out)
	}
	attractdisp2 <- CoordNamesToOrdering(attractdisp,nRows)
	paramssurviedisp2 <- CoordNamesToOrdering(paramssurviedisp,nRows)
#browser()
	voisinage <- ENVIES[[esc]];
    # generate result in arrivdisp
    DistributeACellMultiCppAll(qte=depart[,1],
                                xDep=depart[,2],
                                yDep=depart[,3],
                             voisRow=voisinage$row, 
                             voisCol=voisinage$col, 
                             voisd=voisinage$d, 
                             voisPot=voisinage$pot, 
                             attractivities= attractdisp2[,1], 
                             paramSurv= paramssurviedisp2[,1],
                             nRows=nRows,
                             nCols=nCols,
                             survivalName=PARAMETRES[[e]]$dispori[[esc]]$fonctionsurvie,
                             arrivDisp=arrivdisp
                             )
    browser(expr=sum(arrivdisp)<0)
'
sourceCpp("musclr/distribueunecase.cpp")
library("rbenchmark")
benchmark(replications=2,DistributeACellMultiCpp(depart=depart,
			voisRow=voisinage$row, 
			voisCol=voisinage$col, 
			voisd=voisinage$d, 
			voisPot=voisinage$pot, 
			attractivities= attractdisp2[,1], 
			paramSurv= paramssurviedisp2[,1],
			nRows=nRows,
			nCols=nCols,
			survivalName=PARAMETRES[[e]]$dispori[[esc]]$fonctionsurvie,
			arrivDisp=arrivdisp
			))
benchmark(replications=2,DistributeACellMultiCppAll(qte=depart[,1],
                                                     xDep=depart[,2],
                                                     yDep=depart[,3],
			voisRow=voisinage$row, 
			voisCol=voisinage$col, 
			voisd=voisinage$d, 
			voisPot=voisinage$pot, 
			attractivities= attractdisp2[,1], 
			paramSurv= paramssurviedisp2[,1],
			nRows=nRows,
			nCols=nCols,
			survivalName="homogenePower",
			arrivDisp=arrivdisp
			))
'

#on resimplifie
	arriveesimpl<-aggregate(arrivdisp, by= ESPACECOMPL[[paste("compllev", nivdisp, sep="")]][,"reprby", drop=FALSE], mean)
	rownames(arriveesimpl)<-arriveesimpl$reprby
	arriveesimpl<-arriveesimpl[rownames(ESPACE[[paste("lev", nivdisp, sep="")]]),]

#on remet au bon niveau
	#si nivdisp>nivpop, on fait redescendre en fonction de l attractivite: pas repartition homogene dans souspixel mais proportion de la pop attribuee au souspixel 
	#calculee en fonction de l attractivite relative du souspixel par rapport a l attractivite moyenne du gros pixel
	if (nivdisp>nivpop) 
	{
		facteurmult<-attractpop$attractivity/attractdisp[ (ESPACE[[paste("lev", nivpop, sep="")]][,paste("pixfather", nivdisp,sep="")]) , "attractivity"]/((TC^2)^(nivdisp-nivpop))
		facteurmult[is.na(facteurmult)]<-0
		arrivpop<-matrix(
			arriveesimpl[ ESPACE[[paste("lev", nivpop, sep="")]][,paste("pixfather", nivdisp,sep="")] ,-1]*facteurmult,
			ncol=1
		)
	} else {arrivpop<-as.matrix(changelevel(arriveesimpl[,-1, drop=FALSE], nivdep=nivdisp, nivarr=nivpop, method="countable")) ; arrivpop<-arrivpop[rownames(ESPACE[[paste("lev", nivpop, sep="")]]),,drop=FALSE]}
	colnames(arrivpop)<-esc
    
    if(OPTIONSIMU$printwhat>0) cat("vol",e,s,"departing:",sum(depart),"; arriving:",sum(arrivpop),"\n")
#     if(e == "parasitoide"){
#         #browser()
#         par(mfcol=c(2,3))
#         ImagePodyam(depart)
#         # ImagePodyam(adisperser)
#         ImagePodyam(attractdisp2$attractivity,rownames(attractdisp2))
#         ImagePodyam(paramssurviedisp2$a,rownames(paramssurviedisp2))
#         require("spatDataManagement")
#         plot(voisinage$col,voisinage$row,col=XToColors(log(voisinage$pot+1)))
#         ImagePodyam(arriveesimpl$x,arriveesimpl$reprby)
#         ImagePodyam(arrivpop)
#     }

    return(arrivpop)
}

dispersionorientee<-function(e.s)
{
  splits<-strsplit(e.s, split=".", fixed=TRUE)
  e<-unlist(lapply(splits, "[[", 1))
  s<-unlist(lapply(splits, "[[", 2))
  listesc<-names(PARAMETRES[[e]]$dispori)
  listesc<-listesc[grepl(pattern=e.s, x=listesc)]
  yenatil<-unlist(lapply(listesc, function(x) {
	splits<-strsplit(x, split=".", fixed=TRUE)
	esp<-unlist(lapply(splits, "[[", 1))
	sta<-unlist(lapply(splits, "[[", 2))
	cond<-unlist(lapply(splits, "[[", 3))
	condition<-eval(PARAMETRES[[esp]]$dispori[[x]]$parsed_condition)
	return(sum(POPULATIONS[[e]][condition, sta])>0)
  }))
  if (sum(yenatil)>0) {  
      if (OPTIONSIMU$printwhat>2) print( paste("oriented dispersal of", paste( listesc[yenatil], collapse =",")))
      # toto<-mapply(vol, listesc[yenatil], SIMPLIFY=FALSE)
      # result.es<-matrix(rowSums(as.data.frame(toto)),nrow(toto[[1]]), ncol(toto[[1]])) 
      if(sum(yenatil)>1){
          stop("Condition of unicity of yenatil true not observed, need to put back the commented lines above")
      }
      result.es <- vol(listesc[yenatil])
      POPULATIONS[[e]][,s] <<- result.es[,1]
  }
  return()
}

#NB on ne peut pas utiliser exactement meme schema que autres fonctions car pour dispersion, il faut sommer 
# toutes les propagules qui arrivent dans un pixel, qqsoit leur condition de depart
#en plus, pas de fonction ni de parametres car ENVIES sont calculees au debut de la simulation
# ou quand on modifie les parametres en cours de simu (en fait non, envies calculees a chaque pas de temps)
dispori<-function(ICI) 
{
  toto<-PARAMETRES
  names(toto)<-NULL
  noms<-unlist(lapply(toto, function(x) {
	names(x$dispori)
	}), recursive=FALSE)
  if(length(noms)>0) 
  {
	splits<-strsplit(noms, split=".", fixed=TRUE)
	especes<-unlist(lapply(splits,"[[", 1))
	stades<-unlist(lapply(splits,"[[", 2))
	#on garde que les esp.sta
	noms<-unique(paste(especes, stades, sep="."))
	# temps<-system.time(lapply(noms, dispersionorientee))[2]
	# return(temps)
    for(nom in noms){
        dispersionorientee(nom)
    }

	# lapply(noms, dispersionorientee)
  }
}


} # fin de if (length(paramsorientes)>0) 


############################################################# dispersion via matrice calculee de facon externe (p exple par califlopp)
####initialisation des matrices de dispersion (matrice a deux dim: dimension 1 = from, dimension 2= to, contient la probabilite qu une spore de pixel 1 atterrisse dans pixel 2.
#on cherche les e.s.c qui ont dispersion matrice fromto
toto<-lapply(PARAMETRES, "[[", "dispfromto")
names(toto)<-NULL
paramsfromto <- unlist(toto,recursive=FALSE)
if (length(paramsfromto)>0) 
{
  #on charge une bonne fois pour toutes les resultats de califlopp pour tous les Rdata appeles par bioag qui ont dispersion fromto
  rdatas<-unique(unlist(lapply(paramsfromto,"[[", c("parametres", "rdata"))))
  names(rdatas)<-rdatas
  FROMTO<-lapply(rdatas, function(x) {
	load(x)
	contient<-ls()
	if (length(setdiff(contient, "x"))>1) 
		stop(paste("error in reading external dispersal matrix: more than one object in", x)) else
		return(get(contient[1]))
	})
 #FROMTO est une liste (un element par rdata lu) de matrices ou de listes (nombre arbitraire d elements et niveaux de sous-listes), branche finale = matrice carree de dispersion (dim1=de, dim2=a)

indexfromtext<-function(rdata, textindices)
{
  toto<-lapply(textindices,function(x) eval(parse(text=x)))
  return( FROMTO[[rdata]][[unlist(toto)]] )
}
#exple: str(indexfromtext("/Users/Marie_Gosme/Documents/b_entreesSIPPOM/califlopp_region113parcelles.Rdata", textindices=c('sample(names(FROMTO[[1]]),1)', 'sample(names(FROMTO[[c(1,1)]]),1)')))
# str(indexfromtext("/Users/Marie_Gosme/Documents/b_entreesSIPPOM/cali135x135.Rdata", textindices=c('"lev1"', '"dispf3"')))


e.s<-"rouille.S1"
POPULATIONS[["rouille"]][1,"S1"]<-100
POPULATIONS[["rouille"]][3,"S1"]<-100

dispersionfromto <-function(e.s)
{
  e<-unlist(lapply(strsplit(e.s, split=".", fixed=TRUE), "[[", 1))
  s<-unlist(lapply(strsplit(e.s, split=".", fixed=TRUE), "[[", 2))
  paramse<-PARAMETRES[[e]]$dispfromto
  paramse <-paramse[grepl(paste(e,s,sep="."), names(paramse), fixed=TRUE)]
  conditions<-lapply(paramse, function(x) eval(x$parsed_condition) & POPULATIONS[[e]][,s]>0)
  yenatil<-unlist(lapply(conditions, function(x) sum(x)>0))
  if (sum(yenatil)>0) {
	listesc<-names(conditions)[yenatil] ; names(listesc)<-listesc
	if (OPTIONSIMU$printwhat>2) print( paste( listesc ))
	paramse<-paramse[listesc]
	# indices des pixels qui ont des spores partantes
	qui<-lapply(conditions[listesc], which, arr.ind=TRUE)
	# on fait matrice de tous les pixels qui ont spores partantes vers tous les pixels du paysage en allant piocher les matrices necessaires dans FROMTO
	# si possible en utilisant les noms de pixels comme indices
	
	touscas<-do.call(rbind, lapply(listesc, function(esc) 
	{
		if (!is.null(names(qui[[esc]]))) indices<-names(qui[[esc]]) else indices<-qui[[esc]]
		do.call(paramse[[esc]]$fonction, paramse[[esc]]$parametres)[indices,, drop=FALSE] 
	}))
	#il faut multiplier les partantes par le nb de pixels representes
	niv<-PARAMETRES[[e]]$niv
	partantes<-matrix(POPULATIONS[[e]][rownames(touscas),s]*ESPACE[[paste("lev", niv, sep="")]][rownames(touscas), "represents"], nrow=1)
	POPULATIONS[[e]][,s] <<- (partantes %*% touscas)/ESPACE[[paste("lev", niv, sep="")]][, "represents"]
  }
  return()
}


#NB on peut pas utiliser exactement meme schema que autres fonctions car pour dispersion, il faut sommer toutes les propagules qui arrivent dans un pixel, qqsoit leur condition de depart
#en plus, pas de fonction ni de parametres car FROMTO calcule en dehors de la simulation 
dispfromto<-function() 
{
  toto<-PARAMETRES
  names(toto)<-NULL
  noms<-unlist(lapply(toto, function(x) {
	names(x$dispfromto)
	}), recursive=FALSE)
  if (OPTIONSIMU$printwhat>2) print(paste("matrix dispersal of", paste(noms, collapse=" ")))
  if(length(noms)==0) {
      if (OPTIONSIMU$printwhat>2) print("no organism has matrix dispersal")
      return()
  }else{
  splits<-strsplit(noms, split=".", fixed=TRUE)
  especes<-unlist(lapply(splits,"[[", 1))
  stades<-unlist(lapply(splits,"[[", 2))
  #on garde que les esp.sta
  noms<-unique(paste(especes, stades, sep="."))
  # temps<-system.time(lapply(noms, dispersionfromto))[2]
  # return(temps)
  lapply(noms, dispersionfromto)
  }
}


} #fin de if (length(paramsfromto)>0) 


# =======================================
# Fonctions de service
# =======================================

#' attributes the values in "val" according to row and col values
#' @description Use "col" and "row" to add "val" to the corresponding 
#'     item in matrixInLine
#' @param row The vector of row indexes of the values 
#' @param col The vector of col indexes of the values 
#' @param val The vector of values to add to the matrix
#' @param matrixInLine the vector of values in the matrix filled by columns
#' @return 0 usual code to tell everything went right
#' @details Careful no checks of the adequacy of row/col for matrixInLine (risk of overflow)
#' @examples
#' nCols <- 4
#' nRows <- 3
#' rows <- c(1,1,2,3,2)
#' cols <- c(1,2,2,3,4)
#' vals <- c(1,4,2,3,5)
#' vectArrive <- mat.or.vec(nRows*nCols,1)
#' AffectToXY(rows,cols,vals,vectArrive,nCols,nRows)
#' matArrive <- matrix(vectArrive,nrow=nRows,byrow=FALSE)
#' library(testthat)
#' expect_equal(as.vector(matArrive),c(1,0,0,4,2,0,0,0,3,0,5,0))
#' @export
cppFunction('int AffectToXY(NumericVector row, NumericVector col, NumericVector val, NumericVector matrixInLine, int nCols, int nRows){
	    for(int i=0 ; i< row.size(); i++){
		int iMatrixInLine = (row[i]-1)+(col[i]-1)*nRows;
		matrixInLine[iMatrixInLine] += val[i];
	    }
	    return 0;
}')
# ------------------------
# import functions from Cpp
# ------------------------
# also see "distribueunecase.cpp" for extensive check against former implementation
sourceCpp("distribueunecase.cpp")

#-------------------------
# test modulo function
#-------------------------
expect_equal(modulo(3,2),1)
expect_equal(modulo(-1,99),98)

#-------------------------
# tests DistributeACellCpp
#-------------------------
# tests dispersal
qxy <- c(4,1,1) # quantity, x, y
voisRow <- c(-1,0,-1,1)
voisCol <- c(-1,-1,0,1)
voisd <- rep(1,length(voisRow))
voisPot <- voisd
attractivities <- rep(1,12)
paramSurv <- attractivities
nRows <- 3
nCols <- 4
arrivDisp <- arrivDisp0 <- seq(1,12)
arrivDisp <- DistributeACellMultiCppAll(qxy[1],qxy[2],qxy[3],
                                         voisRow,voisCol,voisd,voisPot,
		   attractivities,paramSurv,
		   nRows,nCols,"puissanced",arrivDisp)
expect_equal(arrivDisp,c(1,2,4,4,6,6,7,8,9,11,11,13))
# library("rbenchmark")
# 
# sourceCpp("musclr/distribueunecase.cpp")
# benchmark(replications = 1000000, arrivDisp <- DistributeACellCpp(qxy,voisRow,voisCol,voisd,voisPot,
# 		   attractivities,paramSurv,
# 		   nRows,nCols,"puissanced",arrivDisp))

# Nota: would be cool to be able to pass an R function, the day Rcpp can efficiently cast the output
arrivDisp <- arrivDisp0
qxy <- c(4,3,4)
arrivDisp <- DistributeACellMultiCppAll(qxy[1],qxy[2],qxy[3],voisRow,voisCol,voisd,voisPot,
		   attractivities,paramSurv,
		   nRows,nCols,"puissanced",arrivDisp)
expect_equal(arrivDisp,c(2,2,3,4,5,6,7,9,10,10,12,12))

# tests Potential and attractivities
arrivDisp <- arrivDisp0
qxy <- c(4,2,2)
voisRow <- c(-1,0)
voisCol <- c(-1,-1)
voisd <- c(1,1)
voisPot <- c(2,1)
attractivities <- rep(1,12)
attractivities[1:2] <- c(3,5)
paramSurv <- rep(1,12)

arrivDisp <- DistributeACellMultiCppAll(qxy[1],qxy[2],qxy[3],voisRow,voisCol,voisd,voisPot,
		   attractivities,paramSurv,
		   nRows,nCols,"puissanced",arrivDisp)

arriv <- attractivities[1:2]*voisPot
arriv <- arriv/sum(arriv)
arrivDispTest <- c(1,2,3,4,5,6,7,8,9,10,11,12)
arrivDispTest[1:2] <- qxy[1]*arriv+arrivDispTest[1:2]

expect_equal(arrivDisp,arrivDispTest)

# tests survival
arrivDisp <- arrivDisp0
qxy <- c(4,2,2)
voisRow <- c(-1)
voisCol <- c(-1)
voisd <- c(2.2)
voisPot <- 1
attractivities <- rep(1,12)
paramSurv <- attractivities
paramSurv[1] <- 0.99

arrivDisp <- DistributeACellMultiCpp(matrix(qxy,nrow=1),voisRow,voisCol,voisd,voisPot,
		   attractivities,paramSurv,
		   nRows,nCols,"puissanced",arrivDisp)
expect_equal(arrivDisp,c(1+4*0.99^2.2,2,3,4,5,6,7,8,9,10,11,12))


arrivDisp <- arrivDisp0
depart <- matrix(qxy,nrow=1)
arrivDisp <- DistributeACellMultiCppAll(depart[,1],depart[,2],depart[,3],voisRow,voisCol,voisd,voisPot,
		   attractivities,paramSurv,
		   nRows,nCols,"puissanced",arrivDisp)
expect_equal(arrivDisp,c(1+4*0.99^2.2,2,3,4,5,6,7,8,9,10,11,12))

arrivDisp <- arrivDisp0
arrivDisp <- DistributeACellMultiCppAll(depart[,1],depart[,2],depart[,3],voisRow,voisCol,voisd,voisPot,
		   attractivities,paramSurv,
		   nRows,nCols,"homogenePower",arrivDisp)
expect_equal(arrivDisp,c(1+4*0.99^2.2,2,3,4,5,6,7,8,9,10,11,12))


# sourceCpp("musclr/distribueunecase.cpp")
nPoints <- 100000
nRep <- 10
arrivDisp <- arrivDisp0
#library("rbenchmark")
depart <- matrix(rep(qxy,nPoints),nrow=nPoints,byrow=T)
#benchmark(replications=nRep,arrivDisp <- DistributeACellMultiCpp(depart,voisRow,voisCol,voisd,voisPot,
#		   attractivities,paramSurv,
#		   nRows,nCols,"puissanced",arrivDisp))
#arrivDisp <- arrivDisp0
#benchmark(replications=nRep,arrivDisp <- DistributeACellMultiCppAll(depart[,1],depart[,2],depart[,3],voisRow,voisCol,voisd,voisPot,
#		   attractivities,paramSurv,
#		   nRows,nCols,"puissanced",arrivDisp))
#arrivDisp <- arrivDisp0
#benchmark(replications=nRep,arrivDisp <- DistributeACellMultiCppAll(depart[,1],depart[,2],depart[,3],voisRow,voisCol,voisd,voisPot,
#		   attractivities,paramSurv,
#		   nRows,nCols,"homogenePower",arrivDisp))






