#attention: forcage a ete modifie: parametre valeuvide renomme en nareplacement
#############fonctions generiques qui sont utiles pour tous les processus

evalICI<-function(expr) eval(parse(text=expr), envir=ICI)

####conversions temporelles (attention, le modele fonctionne en "annee" de 360 jours, 1 "mois" = 30 jours

#NOMSMOIS<-c("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "décembre")
NOMSMOIS<-month.name #month.name est un objet R qui contient les noms des mois en anglais

convertit1<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, from=TIMESTEP, to=TIMESTEP, elapsedtime=FALSE)
{
#fonction qui permet de convertir entre les differents pas de temps possibles
#attention: par defaut, convertit1 renvoie une date dans l annee (sauf si to=year), pour avoir un nombre de pas de temps de simulation, mettre elapsedtime=TRUE
#NB pour gagner du temps en ne pas avoir a refaire conversion a chaque fois, conversion des scenarios est faite une bonne fois pour toutes dans restart donc on peut alors utiliser convertit2 qui ne fait que boucler les annees
  #si on est en elapsedtime, il suffit de division entiere si on passe de petit pas de temps a grand pas de temps (pas de temps pas fini ne compte pas) ou de multiplier si on passe de grand pas de temps a pas de temps plus petit
  if (elapsedtime) {
	conversions<-matrix(c(
		"t",		"t %/% 10",	"t %/% 30",	"t %/% 360",
		"t *10",	"t",		"t %/% 3",	"t %/% 36",
		"t *30",	"t *3",		"t",		"t %/% 12"
	),byrow=TRUE, nrow=3, dimnames=list(from=c("day", "10days", "month"), to=c("day", "10days","month", "year")))
  } else {  #si on est pas en elapsed, il faut recaler par rapport au moment dans l annee et au debut de la simulation
	t<- t-2+ debutsimu #car t est incremente au debut du pas de temps donc si on veut que le premier pas de temps soit le pas de temps de debutsimu
	conversions<-matrix(c( #attention: si on voit erreur ici, il faut aussi corriger dans init
		"(t) %% 360 + 1",	"(t %/% 10) %% 36 + 1",		"(t %/% 30) %% 12 + 1",		"t %/% 360 +1 ",
		"(t *10) %% 360 + 5",	"(t) %% 36 + 1",		"(t %/% 3) %% 12 + 1",		"t %/% 36 + 1",
		"(t *30) %% 360 + 15",	"(t *3) %% 36 + 2",		"(t) %% 12 + 1",		"t %/% 12 + 1"
	), byrow=TRUE, nrow=3, dimnames=list(from=c("day", "10days", "month"), to=c("day", "10days","month", "year")))
  }
  return(eval(parse(text=conversions[from, to])))
}

convertit2<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, timestep=TIMESTEP)
{
# nouvelle fonction convertit2: permet de passer de SIMUTIME a nb de TIMESTEP dans l annee: plus simple donc plus rapide que convertit1, a utiliser lorsque t est dans meme unite que TIMESTEP, i.e. en particulier pour les scenarios de climat qui sont convertis une bonne fois pour toutes au moment de leur lecture
	t<- t-2+ debutsimu #car t est incremente au debut du pas de temps donc si on veut que le premier pas de temps soit le pas de temps de debutsimu
	nbstepcycle<-c(360, 36, 12)
        names(nbstepcycle)<-c("day", "10days", "month")
	return(t %% nbstepcycle[timestep] + 1)
}

#fonctions qui renvoient le moment dans l annee en fonction de t, de debutsimu et de timestep 
mois<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, timestep=TIMESTEP)
{
  return(convertit1(t, debutsimu, from=timestep, to="month"))
}
jour<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, timestep=TIMESTEP)
{
  return(convertit1(t, debutsimu, from=timestep, to="day"))
}



#fonctions qui renvoient le climat actuel (peuvent etre utilisees par differentes autres fonctions)
temperature<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, scenar=SCENARIOTEMP$Celsius, wrap=SCENARIOTEMP$wrap)
{
   if (wrap) return( scenar[convertit2(t, debutsimu)]) else return(scenar[t])
}
GDD<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, scenar=SCENARIOTEMP$GDD, wrap=SCENARIOTEMP$wrap)
{
  if (wrap) return( scenar[convertit2(t, debutsimu)]) else return(scenar[t])
}
pluie<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, scenar=SCENARIORAIN$mm, wrap= SCENARIORAIN$wrap)
{
   if (wrap) return( scenar[convertit2(t, debutsimu)]) else return(scenar[t])
}
vitessevent<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, scenar=SCENARIOWIND$mpars, wrap= SCENARIOWIND$wrap)
{
   if (wrap) return( scenar[convertit2(t, debutsimu)]) else return(scenar[t])
}
directionvent<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, scenar=SCENARIOWIND$degre, wrap= SCENARIOWIND$wrap)
{
   if (wrap) return( scenar[convertit2(t, debutsimu)]) else return(scenar[t])
}
lumiere<-function(t=SIMUTIME, debutsimu=DEBUTSIMU, scenar=SCENARIORAY$Joule, wrap= SCENARIORAY$wrap)
{
   if (wrap) return( scenar[convertit2(t, debutsimu)]) else return(scenar[t])
}

STempMETEO<-function(dayi, dayf, Tseuil=0) 
{
#dayi et dayf doivent etre en jours de simulation ou en Date et METEO doit etre en jours
	if (dayi<=0 | dayi>dayif) return(0) else 
	if (class(dayi)=="Date") return(sum(pmax(0, METEO[METEO$date>=dayi & METEO$date<=dayf,"Temperature"]-Tseuil))) else 
	return(sum(pmax(0, METEO[dayi:dayf,"Temperature"]-Tseuil)))
}
STempSCENARIO<-function(dayi, dayf, Tseuil=0, debutsimu=DEBUTSIMU, scenar=SCENARIOTEMP$Celsius, wrap=SCENARIOTEMP$wrap, timestep=TIMESTEP) 
{
#dayi et dayf doivent etre en jours julien ou mois ou 10days depuis janvier
	scenar<-pmax(scenar-Tseuil, 0)
	if (timestep =="day") {mult=1; unan=360} else if (timestep =="10days") {mult=10; unan =36} else {mult=30; unan =12}
	if (!wrap) { dayi <-dayi+debutsimu+(unan*SIMUTIME%/% unan); dayf <-dayf+debutsimu+(unan*SIMUTIME%/%unan) ; return(sum(scenar[dayi:dayf])*mult)
	} else { #i.e. wrap=TRUE: scenario en jours juliens (ou mois ou 10days)
		if(dayi<dayf)  return( sum(scenar[dayi:dayf])*mult) else { #i.e. a cheval sur 2 annees
			return( (sum(scenar[1:dayf]) + sum(scenar[dayi:unan]))*mult)
		}
	}
}

################ fonction qui renvoie l aggrégation ou la désaggrégation du vecteur numeric (si method=countable ou uncountable) ou character (si method=mostfrequent ou allproportions ou firstone) d entree d un niveau de hierarchie spatiale vers un autre
##### avec quatre possibilites pour method: 
changelevel<- function(df, nivdep, nivarr, method=c("uncountable", "countable", "mostfrequent", "allproportions", "proportionof", "firstone"), category=NULL, na.rm=FALSE)
{
#method="uncountable": la quantite est non comptable (p exple temperature): on prend la moyenne pour aggregation, et on repete le meme chiffre pour desagregation
#method="countable":  la quantite est comptable (p exple nombre d individus): on prend la somme pour aggregation, et on divise la quantite equitablement entre pixels fils pour desagregation
#method="mostfrequent": pour l aggregation, on ne garde que la qualite la plus frequente parmi les pixels fils, pour l aggregation on met la meme chose partout
#method= "allproportions": pour l aggregation, on cree matrice contenant les proportions de chaque qualite posible, ne peut pas etre utilise pour la desagregation
#method= "proportionof": pour l aggregation, on calcule la proportion d un certain type de pixel (parametre category)
#method= "firstone": pour l aggregation, on garde juste la premiere valeur trouvee parmi les sous-pixels (donc a n nutiliser que si on sait qu ils sont tous pareils, par exemple pour passer de typeculture de niveau 1 a typeculture de niveau parcelle
#category= uniquement si method= "proportionof": soit character donnant la categorie dont on veut avoir la proportion, soit expression non evaluee du type parse(text='x %in% c("ble1_conv", "ble2_conv")') donnant la condition dont on veut la proportion de realisation: x est une colonne de df (car on fait lapply(df, function(x) eval(category)) )
#browser()
    if (nivdep==nivarr) result<-df else
        if (nivdep>nivarr) #niveaux socio doivent etre par ordre alphabetique donc pour en etre certain, les appeler apatch et bfarm par exemple
        { ########################################## desaggregation
            if(suppressWarnings(is.na(as.numeric(nivdep)) & !(nivarr=="1" | is.na(as.numeric(nivdep))))) stop("desaggregation de niveau socio a niveau geographique autre que 1 n est pas encore implementee")
            if (method %in% c("uncountable", "mostfrequent", "firstone"))
            { #on selectionne juste plusieurs fois chaque element
                result<-df[ ESPACE[[paste("lev", nivarr, sep="")]][,paste("pixfather", nivdep, sep="")], ,drop=FALSE]
                row.names(result)<-row.names(ESPACE[[paste("lev", nivarr, sep="")]])
            } else if (method =="countable") 
            {  #on selectionne plusieurs fois chaque element et on le divise par le nombre de pixels fils (repartition equitable): 2 cas separes: nivdep= soit  niveau "geographique" soit niveau "sociologique"
                if(suppressWarnings(!is.na(as.numeric(nivdep)))) diviseur<-(TC^2)^(as.numeric(nivdep)-as.numeric(nivarr)) else 
                    diviseur<-ESPACE[[paste("lev", nivdep, sep="")]][ ESPACE[[paste("lev", nivarr, sep="")]][,paste("pixfather", nivdep, sep="")] , paste("nbpixlev", nivarr, sep="")]
            result<-df[ ESPACE[[paste("lev", nivarr, sep="")]][,paste("pixfather", nivdep, sep="")], ,drop=FALSE] / diviseur
            row.names(result)<-row.names(ESPACE[[paste("lev", nivarr, sep="")]])
            } else stop(paste( "disaggregation with method", method, "is not defined yet"))
        } else { ########################################## aggregation
          browser(expr=(any(grepl("levapatch",nivarr))))
            if(suppressWarnings(is.na(as.numeric(nivarr)) & nivarr!="ZZZ" & !(nivdep=="1" | is.na(as.numeric(nivdep))))) stop("aggregation de niveau geographique autre que 1 a niveau socio n est pas encore implementee")

            # combien ceux qui representent plusieurs pixels au niveau de depart en representent-ils *au niveau d'arrivee*
            if(suppressWarnings(!is.na(as.numeric(nivarr)))) { #nivarr est niveau geographique: on reste de pixel a pixel
                poids<-pmin(ESPACE[[paste("lev", nivdep, sep="")]][,"represents"], (TC^2)^(as.numeric(nivarr)-as.numeric(nivdep)))
                diviseur<-(TC^2)^(as.numeric(nivarr)-as.numeric(nivdep))
            } else { #nivarr est socio ou landscape
                if (suppressWarnings(!is.na(as.numeric(nivdep)))) #nivdep est geographique et nivarr="ZZZ" 
                {
                    poids<-ESPACE[[paste("lev", nivdep, sep="")]][,"represents"] 
                } else {
                    poids<- ESPACE[[paste("lev", nivdep, sep="")]][,"nbpixlev1"]
                } 
                if (nivarr=="ZZZ") 
                  diviseur<-(TC^(NBNIV-1)*TP)^2 else diviseur<-ESPACE[[paste("lev", nivarr, sep="")]][,"nbpixlev1"]
            }

            if (method =="uncountable")
            { # on fait moyenne de tous les pixels fils, ponderee par l importance du pixel dans le total
                df<-df*poids
                if (nivarr=="ZZZ") result=matrix(colSums(df, na.rm=na.rm)/diviseur, nrow=1, dimnames=list(NULL, colnames(df))) else {
                    #on additionne par pixels du niveau d arrivee
                    group<-ESPACE[[paste("lev", nivdep, sep="")]][,paste("pixfather", nivarr, sep="")]
                    result<-rowsum(df, group=group, na.rm=na.rm)
                    #on remet dans le bon ordre
                    result<-result[row.names(ESPACE[[paste("lev", nivarr, sep="")]]),, drop=FALSE] 
                    #on redivise par le nombre de pixels de nivdep qu il y a dans un pixel de nivarr
                    result<-result/diviseur
                }
            } else if (method =="countable")
            {  # on fait la meme chose sauf qu on ne divise pas
              #browser(exp=(any(grepl("apatch",nivdep))))
                if(nivdep=="apatch") df<-df else {
                df<-df*poids}
                if (nivarr=="ZZZ") result=matrix(colSums(df, na.rm=na.rm), nrow=1, dimnames=list(NULL, colnames(df))) else {
                    #on additionne par pixels du niveau d arrivee
                    group<-ESPACE[[paste("lev", nivdep, sep="")]][,paste("pixfather", nivarr, sep="")]
                    result<-rowsum(df, group=group, na.rm=na.rm)
                    #on remet dans le bon ordre
                    result<-result[row.names(ESPACE[[paste("lev", nivarr, sep="")]]),, drop=FALSE] 
                }
            } else if (method =="firstone")
            {
                result<-aggregate(df, by=ESPACE[[paste("lev", nivdep, sep="")]][,paste("pixfather", nivarr, sep=""), drop=FALSE], function(x) x[1])
                row.names(result)<-result[,paste("pixfather", nivarr, sep="")]
                #on enleve la variable cree par aggregate
                result[,paste("pixfather", nivarr, sep="")]<-NULL
                #on remet dans le bon ordre
                result<-result[row.names(ESPACE[[paste("lev", nivarr, sep="")]]),, drop=FALSE] 
            } else if (method =="mostfrequent")
            { #on prend la qualite qui est la plus abondante dans le pixel de niveau nivarr
                #on trouve groupe
                if(nivarr=="ZZZ") {
                    stop("pas encore fait aggreg mostfrequent dans paysage")
                } else {
                    group<-ESPACE[[paste("lev", nivdep, sep="")]][,paste("pixfather", nivarr, sep="")]
                }
                #on prend la valeur qui a le plus grand poids (=la plus frequente) parmi toutes celles du groupe
                result<-do.call(rbind,by(
                                         data=cbind(poids, df),
                                         INDICES=group,
                                         FUN=function(df) sapply(df[,-1, drop=FALSE], function(cat) {
                                                                 nbs<-rowsum(df$poids, group=as.character(cat))
                                                                 rownames(nbs)[which.max(nbs)]
}),
                                         simplify=FALSE
                                         ))
                #on remet dans le bon ordre
                result<-result[row.names(ESPACE[[paste("lev", nivarr, sep="")]]),, drop=FALSE] 
                result<-as.data.frame(result)
                #si initialement df avait des numeric, on les remet en numeric
                result[,unlist(lapply(df, class))=="numeric"]<-lapply(result[,unlist(lapply(df, class))=="numeric", drop=FALSE], as.numeric)
            } else if (method =="allproportions") #attention cette methode est tres longue et cree plein de variables s il y a beaucoup de categories d occupation du sol
            { #on calcule les proportions de CHAQUE categorie, NB: si on n est interesse que par une categorie, faire "proportionof"
                #on remplace chaque colonne de df par un facteur (pour que les categories non representees dans un pixel soient quand meme prises en compte)
                df[,]<-as.data.frame(lapply(df, factor))
                #on trouve groupe
                if(nivarr=="ZZZ") stop("pas encore fair aggreg allproportions dans paysage") else group<-ESPACE[[paste("lev", nivdep, sep="")]][,paste("pixfather", nivarr, sep="")]
                #on calcule proportion de chaque categorie dans chaque groupe pour chaque variable
                result<-do.call(rbind,by(
                                         data=cbind(poids, toto), 
                                         INDICES=group, 
                                         FUN= function(df) {
                                             listeparcolonne<-lapply(
                                                                     df[,-1, drop=FALSE], 
                                                                     function(cat) {
                                                                         freq<-(rowsum(df$poids, group=cat)/sum(df$poids))[,1]
                                                                         manq<-setdiff(levels(cat), cat)
                                                                         ajout<-rep(0, length(manq));  names(ajout)=manq
                                                                         return(c(freq, ajout))
                                                                     }
                                                                     )
                                             return(as.data.frame(t(as.matrix(unlist(listeparcolonne)))))
                                         },
                                         simplify=FALSE
                                         ))
                #on remet dans le bon ordre
                result<-result[row.names(ESPACE[[paste("lev", nivarr, sep="")]]),, drop=FALSE] 
                colnames(result)<-paste("prop", colnames(result), sep=".")
            } else if (method =="proportionof")
            { #on calcule la proportion d une categorie particuliere
                #on evalue l expression donnant la condition
                if(is.expression(category)) df[,]<-lapply(df, function(x) eval(category)) else df[,]<-lapply(df, function(x) x==category)
                #on la moyenne sur les pixels du niveau d arrivee
                result<-changelevel(df, nivdep=nivdep, nivarr=nivarr, method="uncountable")
                #on cree nom de colonne en enlevant les caracteres speciaux
                colnames(result)<-paste("prop", gsub(".", "", colnames(df), fixed=TRUE), gsub(".", "", make.names(as.character(category)), fixed=TRUE), sep=".")
            } else stop(paste( "aggregation with method", method, "is not defined yet"))
        }
    return(result)
}

#exple: cbind(changelevel(ESPACE[["lev1"]][,c("value"), drop=FALSE], nivdep=1, nivarr=3, method=c("uncountable"))[1:20,,drop=FALSE],
#changelevel(ESPACE[["lev1"]][,c("value"), drop=FALSE], nivdep=1, nivarr=3, method=c("countable"))[1:20,,drop=FALSE],
#changelevel(ESPACE[["lev1"]][,c("value"), drop=FALSE], nivdep=1, nivarr=3, method=c("mostfrequent"))[1:20,,drop=FALSE],
#changelevel(ESPACE[["lev1"]][,c("value"), drop=FALSE], nivdep=1, nivarr=3, method=c("allproportions"))[1:20,,drop=FALSE],
#changelevel(ESPACE[["lev1"]][,c("value"), drop=FALSE], nivdep=1, nivarr=3, method=c("proportionof"), category="5")[1:20,,drop=FALSE],
#changelevel(ESPACE[["lev1"]][,c("value"), drop=FALSE], nivdep=1, nivarr=3, method=c("proportionof"), category=parse(text='x %in% c(5,8)'))[1:20,,drop=FALSE]
#)



#####differentes fonctions d incrementeur de compteur
degreedays<-function(previous, base=0, timestep= TIMESTEP)
{
    if (timestep=="month") 
        return( previous + max(0, temperature()-base)*30 ) else 
            if (timestep=="day") 
                return( previous + max(0, temperature()-base) ) else 
                    if (timestep=="10days") 
                        return( previous + max(0, temperature()-base)*10 ) else 
                            stop(paste("erreur dans degreedays: timestep ", timestep, "n est pas defini"))
}
WOSRGDD<-function(previous, base=0, timestep= TIMESTEP)
{ 
    if (timestep=="month") 
        return( previous + max(0, GDD()-base)*30 ) else 
            if (timestep=="day") 
                return( previous + max(0, GDD()-base) ) else 
                    if (timestep=="10days") 
                        return( previous + max(0, GDD()-base)*10 ) else 
                            stop(paste("erreur dans degreedays: timestep ", timestep, "n est pas defini"))
}
if (i==1) {
    print("i égale 1")
}else{
    print("i est différent de 1")
}
days<-function(previous, timestep=TIMESTEP)
{
    if (timestep=="month") 
        return( previous + 30 ) else 
            if (timestep=="day")  
                return( previous + 1 ) else 
                    if (timestep=="10days")  
                        return( previous + 10 ) else 
                            stop(paste("erreur dans days: timestep est:", timestep))
}

never<-function()
{
    return(-Inf)
}

onlyonce<-function()
{
    return(Inf)
}

tempthreshold<-function(threshold) 
{
    if (temperature() < threshold) return(0) else return(Inf)
}

degreedays0january<-function(previous, base=0, timestep= TIMESTEP)
{
    if (any(TIMESTEP=="month" & convertit1(to="day")==15,
            TIMESTEP=="10days" & convertit1(to="day")==5 ,
            TIMESTEP=="day" & convertit1(to="day")==1 
            ) ) return(0) else 
                if (timestep=="month") 
                    return( previous + max(0, temperature()-base)* 30 ) else 
                        if (timestep=="10days") 
                            return( previous + max(0, temperature()-base)* 10 ) else 
                                if (timestep=="day") 
                                    return( previous + max(0, temperature()-base) ) else 
                                        stop(paste("erreur dans degreedays0january: timestep est:", timestep))
}


trigger<-function(parsed_condition,...)
{
    if (eval(parsed_condition)) return(Inf) else return(-Inf)
}





########### fonctions utilisables pour forcage: leur premier argument (nomme x) doit etre un vecteur ou un df, et elles doivent renvoyer un vecteur

linearcombination<-function(x, ..., intercept=0, nareplacement=NA)
{
    #x: data.frame or named vector
    #...: coefficients for each element of X (these arguments must be named after the names of X)
    #intercept: intercept of the linear combination
    #nareplacement: value used at the end to replace NAs
    variables<-names(x)
    dots <- list(...)
    coefs<-as.matrix(unlist(dots[variables]))
    parpixel<- as.matrix(x) %*% coefs + intercept
    parpixel<-drop(parpixel)
    parpixel[is.na(parpixel)]<-nareplacement
    return(parpixel)
}

marche<-function(x, ..., x1, x2, x3, x4, y1, y2, y3)
{
    #...: fake arguments (just because forcage will pass both the dataframe extracted from milieu (x) and tabinit
    #fonction en _/~\_ 
    #attention: marche SOIT avec x=vecteur et les params=1 chiffre chacun 
    #                  SOIT avec x=1 chiffre et les params=vecteurs (ou un chiffre)
    #                  SOIT avec x et les éventuels params multiples de même longueur
    toto<-y1 + (x-x1)*(y1-y2)/(x1-x2)
    toto[x<x1]<-y1
    toto[x>x2 & x<=x3]<-y2
    toto[x>x3 & x<x4]<-y2 + (x-x3)*(y3-y2)/(x4-x3)
    toto[x>=x4]<-y3
    return(toto)
}

#marche(-3:50, -2, -1, 8, 50, 5,-6,100)
#marche(-3, -5:-2 , -3:0, -1:2, 1:4, 5,-6,100)
#marche(c(5,5,6,6,7), rep(-2,5), rep(-1,5), rep(5,5), c(5,6,8,10,10), 11,-6,100)

recalage<-function(x, ..., mini=0, maxi=1)
{
    #...: fake arguments (just because forcage will pass both the dataframe extracted from milieu (x) and tabinit
    gamme<-range(x)
    if (gamme[1] != gamme[2])  x<- (x-gamme[1])*(maxi-mini)/(gamme[2]-gamme[1])+mini else warning("x does not vary, it could not be rescaled")
    return(x)
}

transfereinfoESPACE<-function( ... , niv)
{
    #...: almost fake arguments: we just need their names to get the variables names
    #niv: level of space where to look for the variables (passed automatically by forcage)
    dots <- list(...)
    return(ESPACE[[paste("lev", niv, sep="")]][,names(dots)])
}

combinetext<-function(x, ...)
{
    #x: data.frame or named vector containing the text to be pasted
    #...: other arguments to pass to function paste 

    dots<-list(...)
    return(do.call(paste, args=c(as.list(x), dots[setdiff(names(dots), names(x))])))
} 

persquaremeter<-function(x, ..., niv)
{
    #x: data.frame (1 column)
    #...: values for the different categories in x (these arguments must be named after the levels of x)
    #niv: level of space where to get the surface (passed automatically by forcage)
    valeursauchoix<-c(...)
    x<-x[,1]
    x[!x %in% names(valeursauchoix)]<-"default"
    if (is.na(suppressWarnings(as.numeric(niv)))) mult<-ESPACE[[paste("lev", niv, sep="")]]$nbpixlev1 * GRAIN^2 else  mult<-(( GRAIN*TC^(as.numeric(niv)-1) )^2)
    return(mult* valeursauchoix[x])
}


#################forcage
forcage<-function(tabinit, niv, nareplacement=NA, cond=TRUE)
{
    # fonction de forcage qui est utilisee pour toutes les fonctions du modèle: 
    ## tous les paramètres biologiques qui dependent du milieu et les parametres de paraminitmilieu et paraminitpop passent par là pour être transformés en fonction d'un état du milieu
    ## (sauf params de dyntransi et de dispiso et paramenvie de dispori, qui, eux peuvent dependre que de condition qualitative) 
    ##tabinit= 1) valeur unique (dépend pas du milieu) OU 2) tableau à 1 ligne et n colonnes (n états qualitatifs du milieu), avec dimension 2 nommee selon une variable du milieu avec la dimension 1 eventuellement nommee selon methode a utiliser pour agreger/desagreger le resultat au niveau voulu (cf changelevel) et row.names=niveau de milieu ou est cherchee la variableavec la dimension 2 nommée= nom de la variable du milieu et colnames=modalités de cette variable pour lesquels le paramètre a une valeur particulière OU 3) tableau à 1 ligne et m colonnes (m variables quantitatives du milieu) , avec dimension 2 nommee selon le nom d une fonctionavec la dimension 1 eventuellement nommee selon methode a utiliser pour agreger/desagreger le resultat au niveau voulu (cf changelevel) et row.names=niveau de milieu ou est cherchee la variableavec la dimension 2 nommée = nom de la fonction qui combinera les différentes colonnes et colnames= noms des variables du milieu à prendre en compte 
    ##niv: niveau auquel on veut avoir resultat ATTENTION: si niv n est pas le meme que le niveau auquel la variable est definie, il faut que dim 1 de tabinit soit nommee selon le niveau ou on la trouve, avec le nom de la dimension 1 = methode d aggregation. NB: on applique d abord forcage a l echelle ou les variables sont definies dans milieu, puis on fait changelevel (donc pas possible d avoir des variables de milieu definies a plusieurs niveaux.
    ##nareplacement: valeur à donner aux pixels du milieu qui ont variable qualitative pas spécifiés dans tabinit (NB: si tabinit contient une valeur "default", c'est cette dernière qui est utilisée), ou valeur à donner à une variable quantitative qui n'existe pas dans MILIEU ou pour remplacer les NA des variables quantitatives
    ##renvoie un vecteur de taille= nb de pixels au niveau niv qui contient les valeurs de tabinit attribuées dans les bons pixels
    if (is.null(dim(tabinit)))
    { #cas1
        #~browser()
        # parpixel<-rep(tabinit, times= dim(MILIEU[[paste("lev", niv, sep="")]][cond,, drop=FALSE])[1])
        parpixel<-rep(tabinit, times= length(MILIEU[[paste("lev", niv, sep="")]][cond,1]))
    } else 
    { 
        #cas 2 ou 3
        if (!is.null(dimnames(tabinit)[[1]])) nivmilieu<-dimnames(tabinit)[1] else nivmilieu<-niv
        methodechangelevel<-names(dimnames(tabinit)[1])
        variableforcage<-names(dimnames(tabinit)[2])
        if (variableforcage %in% names(MILIEU[[paste("lev", nivmilieu, sep="")]]))
        { #cas 2
            categoriesdonnees<-dimnames(tabinit)[[2]]
            if(! "default" %in% categoriesdonnees) tabinit<-cbind(tabinit, default=nareplacement)
            milieu<-as.character(MILIEU[[paste("lev", nivmilieu, sep="")]][cond,variableforcage]) #ça c est au cas ou serait factor
            milieu[is.na(milieu) | ! milieu %in% categoriesdonnees]<-"default"
            parpixel<-data.frame(parpixel=tabinit[1, milieu  ], row.names=row.names(MILIEU[[paste("lev", nivmilieu, sep="")]])[cond])
        } else
        { #cas 3 
            #attention: dans cas 3, on passe a la fonction les arguments suivants:
            #1)sous forme de data.frame nomme "x": les colonnes de milieu qui existent 
            #ET 2) sous forme de liste tous les elements (unitaires) de tabinit (donc si il y a des colonne qui existent dans milieu, il faut que la fonction accepte les ...
            variables<-dimnames(tabinit)[[2]]
            variablesexistantes<-intersect(variables, colnames(MILIEU[[paste("lev", nivmilieu,sep="")]]))
            args<-as.list(as.data.frame(tabinit))
            if (length(variablesexistantes)>0) {
                x<-MILIEU[[paste("lev", nivmilieu,sep="")]][cond,variablesexistantes, drop=FALSE]
                args<-c(list(x=x), args)
            }
            if("niv" %in% names(formals(variableforcage))) args<-c(args, list(niv=nivmilieu))
            parpixel<-do.call(variableforcage, args=args) 
            parpixel<-data.frame(parpixel=parpixel, row.names=row.names(MILIEU[[paste("lev", nivmilieu, sep="")]])[cond])
        }
        parpixel<-changelevel(parpixel, nivdep=nivmilieu, nivarr=niv, method=methodechangelevel)$parpixel
    } 
    return(unname(parpixel)) #unname enleve les noms et dimnames
}

#variableforcage<-"linearcombination"
#tabinit<-matrix(c(1:3, 30), ncol=4, dimnames=list(NULL,combinaisonlineaire=c(paste("X", 1:3, sep=""), "intercept")))
#x<-data.frame(X1=1:10, X2=seq(from=0.1, to=0.9, length=10), X3=rep(0,10))
#do.call(variableforcage, args=c(list(x=x), lapply(tabinit[1,], c)))


# Note: aggregate MUCH slower than rowsum

