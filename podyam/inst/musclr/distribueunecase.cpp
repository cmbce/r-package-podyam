/*
 * =====================================================================================
 *
 *       Filename:  distriebueunecase.cpp
 *
 *    Description:  trials to speed up the distribueunecase calls
 *
 *        Version:  1.0
 *        Created:  21/11/2014 13:32:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Barbu (CB), corentin.barbu@gmail.com
 *        Company:  CCEB, University of Pennsylvania
 *
 * =====================================================================================
 */

#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector puissancedCpp(NumericVector base, double exp){
	return pow(base,exp);
}

// the following is 10% faster than the above
// benchmark(replications=1000,vpow(c(0:10000), 3+c(0:10000)))
// [[Rcpp::export]]
NumericVector vecpow(const NumericVector base, const NumericVector exp) {
    NumericVector out(base.size());
    std::transform(base.begin(), base.end(),
            exp.begin(), out.begin(), ::pow);
    return out;
}


// [[Rcpp::export]]
int modulo(int n, int d){
    return (n%d+d)%d;
}
// [[Rcpp::export]]
NumericVector DistributeACellCppRmix(NumericVector qxy,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName,NumericVector arrivDisp,NumericVector surv,NumericVector fromHere){
    double qte = qxy[0];
    int coordx = qxy[1];
    int coordy = qxy[2];
    NumericVector voisCellNum(voisCol.size());
    double totArrivals = 0;

    // arrivals to each cell from the given cell
    for(int i=0; i<voisRow.size(); i++){
        int row = modulo(voisRow[i] + coordx -1,nRows);
        int col = modulo(voisCol[i] + coordy -1,nCols);
        voisCellNum[i] = row + col * nRows;
    }
    // Better to keep it in the loop
    // carefull Rcpp doesn't like the two on the same line
    fromHere = attractivities[voisCellNum]; 
    fromHere = fromHere* voisPot; 

    if(totArrivals == 0){
        totArrivals = 1.;
    }
    // determine the survival function during the move
    double (*functionPtr)(double,double);
    if(survivalName == "puissanced"){
        functionPtr = &pow;
    }else{
        printf("Survival function chosen not available for efficient dispersal\n");
        return NA_REAL;
    }
    // survival in each arrival cell (function of the distance)
    double quantFact = qte / totArrivals;

    surv = paramSurv[voisCellNum];
    surv = vecpow(surv,voisd);

    fromHere = quantFact * surv * fromHere;
    for(int i=0; i<voisRow.size(); i++){
        arrivDisp[voisCellNum[i]] += fromHere[i];
    }
    return arrivDisp;
}
void DistributeACellCppPuissanced(
        int qte,
        int coordx,
        int coordy,
        int voiSize,
        const NumericVector voisRow, 
        const NumericVector voisCol, 
        const NumericVector voisd, 
        const NumericVector voisPot, 
        const NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName, double * arrivDisp){
        
    int voisCellNum[voiSize];
    double arrivals[voiSize];
    double totArrivals = 0;

    // arrivals to each cell from the given cell
    for(int i=0; i<voisRow.size(); i++){
        int row = modulo(voisRow[i] + coordx -1,nRows);
        int col = modulo(voisCol[i] + coordy -1,nCols);
        voisCellNum[i] = row + col * nRows;
        arrivals[i] = voisPot[i] * attractivities[voisCellNum[i]];
        totArrivals += arrivals[i];
    }
    if(totArrivals == 0){
        totArrivals = 1.;
    }
    double (*functionPtr)(double,double);
    functionPtr = &pow;

    // survival in each arrival cell (function of the distance)
    double quantFact = qte / totArrivals;
    for(int i=0; i<voisRow.size(); i++){
        double surv = functionPtr(paramSurv[voisCellNum[i]],voisd[i]);
        arrivals[i] *= quantFact * surv;
        arrivDisp[voisCellNum[i]] += arrivals[i];
    }
}


// [[Rcpp::export]]
NumericVector DistributeACellCpp(NumericVector qxy,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName, NumericVector arrivDisp){
    double qte = qxy[0];
    int coordx = qxy[1];
    int coordy = qxy[2];
    int voisCellNum[voisRow.size()];
    double arrivals[voisRow.size()];
    double totArrivals = 0;

    // arrivals to each cell from the given cell
    for(int i=0; i<voisRow.size(); i++){
        int row = modulo(voisRow[i] + coordx -1,nRows);
        int col = modulo(voisCol[i] + coordy -1,nCols);
        voisCellNum[i] = row + col * nRows;
        arrivals[i] = voisPot[i] * attractivities[voisCellNum[i]];
        totArrivals += arrivals[i];
    }
    if(totArrivals == 0){
        totArrivals = 1.;
    }
    // determine the survival function during the move
    double (*functionPtr)(double,double);
    if(survivalName == "puissanced"){
        functionPtr = &pow;
    }else{
        printf("Survival function chosen not available for efficient dispersal\n");
        return NA_REAL;
    }
    // survival in each arrival cell (function of the distance)
    double quantFact = qte / totArrivals;
    for(int i=0; i<voisRow.size(); i++){
        double surv = 0;
        surv = functionPtr(paramSurv[voisCellNum[i]],voisd[i]);
        arrivals[i] = quantFact*surv * arrivals[i];
        arrivDisp[voisCellNum[i]] += arrivals[i];
    }
    return arrivDisp;
}

// probability of survival not dependant on arrival cell, only on distance
// [[Rcpp::export]]
NumericVector DistributeACellCppFast(NumericVector qxy,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurvVois,
        int nRows, int nCols, String survivalName, NumericVector arrivDisp){
    double qte = qxy[0];
    int coordx = qxy[1];
    int coordy = qxy[2];
    int voisCellNum[voisRow.size()];
    double arrivals[voisRow.size()];
    double totArrivals = 0;

    // arrivals to each cell from the given cell (toric landscape)
    for(int i=0; i<voisRow.size(); i++){
        int row = modulo(voisRow[i] + coordx -1,nRows);
        int col = modulo(voisCol[i] + coordy -1,nCols);
        voisCellNum[i] = row + col * nRows;
        arrivals[i] = voisPot[i] * attractivities[voisCellNum[i]];
        totArrivals += arrivals[i];
    }
    if(totArrivals == 0){
        totArrivals = 1.;
    }
    
    // survival in each arrival cell (function of the distance)
    double quantFact = qte / totArrivals;
    for(int i=0; i<voisRow.size(); i++){
        arrivals[i] = quantFact* arrivals[i]*paramSurvVois[i];
        arrivDisp[voisCellNum[i]] += arrivals[i];
    }
    return arrivDisp;
}
void DistributeACellCppXFast(const NumericVector qxy,
        const NumericVector voisRow, 
        const NumericVector voisCol, 
        const NumericVector voisd, 
        const NumericVector voisPot, 
        const NumericVector attractivities, // not faster when using an "attractivitiesLoc"
        const NumericVector paramSurvVois,
        int nRows, int nCols, 
        double * arrivDisp){
    int coordx = qxy[1];
    int coordy = qxy[2];
    int voisCellNum[voisRow.size()];
    double arrivals[voisRow.size()];
    double totArrivals = 0;

    // arrivals to each cell from the given cell (toric landscape)
    for(int i=0; i<voisRow.size(); i++){
        int row = modulo(voisRow[i] + coordx -1,nRows);
        int col = modulo(voisCol[i] + coordy -1,nCols);
        voisCellNum[i] = row + col * nRows;
        arrivals[i] = voisPot[i] * attractivities[voisCellNum[i]];
        totArrivals += arrivals[i];
    }
    if(totArrivals == 0){
        totArrivals = 1.;
    }
    
    // survival in each arrival cell (function of the distance)
    double qte = qxy[0];
    double quantFact = qte / totArrivals;
    for(int i=0; i<voisRow.size(); i++){
        arrivals[i] = quantFact* arrivals[i]*paramSurvVois[i];
        arrivDisp[voisCellNum[i]] += arrivals[i];
    }
}
void DistributeACellCppAllC(double qte,
        int coordx,
        int coordy,
        int voiSize,
        const NumericVector voisRow, 
        const NumericVector voisCol, 
        const NumericVector voisd, 
        const NumericVector voisPot, 
        const NumericVector attractivities, // not faster when using an "attractivitiesLoc"
        const NumericVector paramSurvVois,
        int nRows, int nCols, 
        double * arrivDisp){
    int voisCellNum[voiSize];
    double arrivals[voiSize];
    double totArrivals = 0;

    // arrivals to each cell from the given cell (toric landscape)
    for(int i=0; i<voiSize; i++){
        int row = modulo(voisRow[i] + coordx -1,nRows);
        int col = modulo(voisCol[i] + coordy -1,nCols);
        voisCellNum[i] = row + col * nRows;
        arrivals[i] = voisPot[i] * attractivities[voisCellNum[i]];
        totArrivals += arrivals[i];
    }
    if(totArrivals == 0){
        totArrivals = 1.;
    }
    // printf("coordx %i, coordy %i totArrivals: %f\n",coordx,coordy,totArrivals);
    
    // survival in each arrival cell (function of the distance)
    double quantFact = qte / totArrivals;
    // printf("qte: %f ; quantFact: %f\n",qte, quantFact);
    for(int i=0; i<voiSize; i++){
        arrivals[i] = quantFact*arrivals[i]*paramSurvVois[i];
        arrivDisp[voisCellNum[i]] += arrivals[i];
        // if(arrivals[i]>0){
            // printf("i %i, voisCell %i, arr: %f, surv: %f\n",i,voisCellNum[i], arrivals[i],paramSurvVois[i]);
        // }
    }
}
// [[Rcpp::export]]
NumericVector SurvivalFnOfDistance(NumericVector paramSurv,NumericVector voisd,String survivalName){ 
// determine the survival function during the move
    NumericVector surv(voisd.size());
    if(survivalName == "homogenePower"){
        for(int i=0; i<voisd.size(); i++){
            surv[i] = pow(paramSurv[0],voisd[i]);
            // printf("ps: %f survi: %f voisd: %f",paramSurv[0], surv[i],voisd[i]);
        }
    }else{
        printf("Survival function chosen not available for efficient dispersal\n");
        return NA_REAL;
    }
    return surv;
}

// [[Rcpp::export]]
NumericVector GetIsOfNeigh(NumericVector qxy,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName, NumericVector arrivDisp){
    int coordx = qxy[1];
    int coordy = qxy[2];
    // arrivals to each cell from the given cell
    for(int i=0; i<voisRow.size(); i++){
        int row = modulo(voisRow[i] + coordx -1,nRows);
        int col = modulo(voisCol[i] + coordy -1,nCols);
        arrivDisp[i] = row + col * nRows;
    }
    return arrivDisp;
}
// GetIsOfNeigh(qxy=depart[1,],
// [[Rcpp::export]]
NumericVector DistributeACellMultiCpp(NumericMatrix depart,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName, NumericVector arrivDisp){
    for(int iDep = 0; iDep < depart.nrow(); iDep ++){
        arrivDisp = DistributeACellCpp(depart(iDep, _),voisRow,voisCol, 
                voisd,voisPot,attractivities,paramSurv,
                nRows,nCols,survivalName,arrivDisp);
    }
    return(arrivDisp);
}
// [[Rcpp::export]]
NumericVector DistributeACellMultiCppFast(NumericMatrix depart,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName, NumericVector arrivDisp){
    NumericVector surv = SurvivalFnOfDistance(paramSurv,voisd,survivalName);
    // printf("ps %f; surv: %f",paramSurv[0],surv[0]);
    for(int iDep = 0; iDep < depart.nrow(); iDep ++){
        arrivDisp = DistributeACellCppFast(depart(iDep, _),voisRow,voisCol, 
                voisd,voisPot,attractivities,surv,
                nRows,nCols,survivalName,arrivDisp);
    }
    return(arrivDisp);
}
// [[Rcpp::export]]
NumericVector DistributeACellMultiCppXFast(NumericMatrix depart,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName, NumericVector arrivDisp){
    NumericVector surv = SurvivalFnOfDistance(paramSurv,voisd,survivalName);
    double arrivDispLoc[arrivDisp.size()];
    for(int i = 0;i<arrivDisp.size();i++){
        arrivDispLoc[i] = 0;
    }
    for(int iDep = 0; iDep < depart.nrow(); iDep ++){
        DistributeACellCppXFast(depart(iDep, _),voisRow,voisCol, 
                voisd,voisPot,attractivities,surv,
                nRows,nCols,arrivDispLoc);
    }
    for(int i = 0;i<arrivDisp.size();i++){
        arrivDisp[i] += arrivDispLoc[i];
    }
    return(arrivDisp);
}
// [[Rcpp::export]]
NumericVector DistributeACellMultiCppAll(NumericVector qte,NumericVector xDep, NumericVector yDep, 
        NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,
        NumericVector paramSurv,
        int nRows, int nCols, String survivalName,NumericVector arrivDisp){
    double arrivDispLoc[arrivDisp.size()];
    for(int i = 0;i<arrivDisp.size();i++){
        arrivDispLoc[i] = 0;
    }
    NumericVector surv = 0;
    if(survivalName == "homogenePower"){ // same power function applied everywhere
        surv = SurvivalFnOfDistance(paramSurv,voisd,survivalName);
    }

    for(int iDep = 0; iDep < qte.size(); iDep ++){
        if(survivalName == "homogenePower"){
            // printf("qte: %f",qte[iDep]);
            DistributeACellCppAllC(qte[iDep],xDep[iDep],yDep[iDep],
                    voisRow.size(),voisRow,voisCol,voisd,
                    voisPot,attractivities,surv,
                    nRows,nCols,arrivDispLoc);
            double s =0;
            for(int i=0;i<arrivDisp.size();i++) { s += arrivDispLoc[i];}
            // printf("sum: %f\n",s);
        }else if(survivalName == "puissanced"){
            DistributeACellCppPuissanced(qte[iDep],xDep[iDep],yDep[iDep],voisRow.size(),
                    voisRow,voisCol,voisd,voisPot,attractivities,paramSurv,
                    nRows,nCols,survivalName,arrivDispLoc);
        }else{
            printf("Survival function chosen not available for efficient dispersal\n");
            return NA_REAL;
        }
    }
    for(int i = 0;i<arrivDisp.size();i++){
        arrivDisp[i] += arrivDispLoc[i];
    }
    return(arrivDisp);
}
// [[Rcpp::export]]
NumericVector DistributeACellMultiCppRmix(NumericMatrix depart,NumericVector voisRow, NumericVector voisCol, 
        NumericVector voisd, NumericVector voisPot, NumericVector attractivities,NumericVector paramSurv,
        int nRows, int nCols, String survivalName, NumericVector arrivDisp){
    NumericVector surv(voisRow.size());
    NumericVector fromHere(voisRow.size()); // declaring NumericVector is also time consuming
    for(int iDep = 0; iDep < depart.nrow(); iDep ++){
        arrivDisp = DistributeACellCppRmix(depart(iDep, _),voisRow,voisCol, 
                voisd,voisPot,attractivities,paramSurv,
                nRows,nCols,survivalName,arrivDisp,surv,fromHere);
    }
    return(arrivDisp);
}
// [[Rcpp::export]]
NumericVector callFunction(NumericVector x, NumericVector y, Function f) {
    NumericVector res = f(x,y);
    return res;
}

