
##############################2) fonctions qui concernent dynamique
##on sépare la dynamique en 2 parties: 
##la partie "locale" où chaque stade réagit à l environnement du pixel (conditions du milieu trouvées dans MILIEU)
##la partie "transition" où on passe d un stade à un autre, mais là ça ne peut pas être différent dans chaque pixel, par contre peut dependre d une condition (mais limiter le nombre de modalites)

#les fonction de la partie locale doivent renvoyer une matrice de même structure que la matrice de la pop (qui la remplacera)
#les fonctions de la partie transition doivent renvoyer une matrice de transition (de stade à stade)



#############################exemples de fonctions de réponse locale au milieu; ont comme arguments: niv et params (liste de params), utilisent MILIEU[["lev"niv]]
######### elles doivent renvoyer la mortalité de l esp.stade : proportion de la biomasse qui disparait (de 0 a 1)

sensifroid<-function(temp, tminsurvie, toptsurvie, tletale,...)
{
#mortalite due a temperature
#pour que ca marche, il faut que les parametres de sensifroid (en particulier temp) soit entres comme ca (par exemple):
#temp=array(1,dim=c(1,1), dimnames=list(uncountable=2, linearcombination="tmoyloc")) si tmoyloc est definie au niveau 2
#tmin, topt, tletale= temperature minimale pour survie, optimale et letale
  mortalites<-marche(x= temp, 
	x1=tminsurvie, x2=toptsurvie, x3=tletale, x4=tletale,
	y1=1, y2=0, y3=1)
  return(mortalites)
}


reponsefongicuratif<-function(sensi)
{ 
#sensi= vecteur des taux de mortalite
#on limite a 1 (suite a forcage, sensi pourrait etre superieur a 1, 
#p exple si parametre sensi = array(c(1,1,1,1), dim=c(1,4), dimnames=list(NULL, combinaisonlineaire=c("acanto", "amistar", "bravoelite", "menara"))) et que dose de chaque fongi est egale a 1
  mortalites<-pmin(sensi, 1)
  return(mortalites)
}

tauxlogistique<-function(textpopactuelle, r, K)
{
#croissance logistique
#r= taux de croissance intrinseque
#K= carrying capacity
#textpopactuelle= texte exprimant la pop actuelle
#attention: les fonctions de mortality doivent renvoyer des mortalites (car dans principe de PODYAM, biomasse n apparait pas par magie mais augmente par predation de ressources)
# donc pour croisance logistique, on renvoie -croissance
	popactuelle<-eval(parse(text=textpopactuelle[1])) #[1] car passe par forcage
	croissance<-r*(1-popactuelle/K)
	return(-croissance)
}

tauxexponentielle<-function(r)
{
#croissance exponentielle
#r= taux de croissance intrinseque
#attention: les fonctions de mortality doivent renvoyer des mortalites (car dans principe de PODYAM, biomasse n apparait pas par magie mais augmente par predation de ressources)
# donc pour croisance exponentielle, on renvoie -croissance
	return(-r)
}


#######################################exemples de fonctions de transition: 
#elles doivent renvoyer une matrice de transition (qui comporte pas forcément tous les stades: potentiellement que les stades qui changent)

sporulation3param<-function(matransf, tbase, topt, tletale)
{ 
#fonction qui modifie une matrice de transition ayant stades mycélium et spores en fonction de température 
#(température modifie production de spore, mais métabolisme de base du mycélium reste le même)
  #NB: les spores ne survivent jamais d un mois sur l autre, 
  #    production de spores suit fonction "marche"
  #    mycelium disparait par métabolisme (pas modifié par température)
  newmat<-matransf
  metabolisme<- 1-sum(matransf["mycelium",])
  newmat["mycelium", "spore"]<-marche(x=temperature(), y1=0, y2=matransf["mycelium","spore"], y3=0, x1=tbase, x2=topt, x3=tletale, x4=tletale)  
  newmat["mycelium", "mycelium"]<- 1-newmat["mycelium", "spore"]-metabolisme
  return(newmat)
}

transiconstante<-function(matransf)
{
#fonction qui garde matrice de transition constante, quelle qu elle soit
  return(matransf)
}


#################### transiphases
transiphases<-function(condglob, matrices){
        #browser()
#fonction qui renvoie matrice de transition par choix parmi matrices fournies
#en fonction de conditions sur variable GLOBALE (si les conditions ne sont pas mutuellement exclusives, on prend la premiere qui est vraie)

  #on cherche dans quelle phase du cycle on est (si les listes sont nommes, on utilise les noms, sinon on suppose que sont dans bon ordre)
  if (!is.null(names(condglob))) {
	phase<-names(condglob)[unlist(lapply(condglob, function(x) eval(parse(text=x))))][1]
  } else {
	phase<-which(unlist(lapply(condglob, function(x) eval(parse(text=x)))))[1]
  }
  return(matrices[[phase]])
} 
# exemple: 
#transiphases(condglob=list(pasvol= 'mois() %in% c(1:6,11:12)', vol='mois() %in% 7:10'),
#             matrices=list(pasvol =matrix(c(0,0.9,0,0.5,0.4,0,0,0,0), byrow=TRUE,ncol=3, 
#                                          dimnames=list(de=c("jeune", "adulte", "aile"), 
#                                                        a=c("jeune", "adulte", "aile"))), 
#                           vol=matrix(c(0,0.9,0,0.3,0.4,0.2,0.8,0,0.1), byrow=TRUE,ncol=3, 
#                                      dimnames=list(de=c("jeune", "adulte", "aile"), 
#                                                    a=c("jeune", "adulte", "aile")))))




#################### transiphasescompteur
#' @title 
#' @name transiphasescompteur()
#' @description
#' @param phases list of the different stages, see examples for more informations
#' @param e.c species_name.condition used to name the counter
#' @example inst/examples/transiphasescompteur_phases.txt
#' @export
transiphasescompteur <- function(phases, e.c){ 
#hybride entre 
#	transiphase (plusieurs stades simultanes et passage de phase a autre en fonction de condition) 
#	et transicompteurlineaire (un seul stade simultane (sauf premier qui peut coexister avec dernier) et passage en fonction d un compteur qu on incremente)
# on donne des matrices de transition en fonction de phases dans le cycle (donc plusieurs etats simultanes possibles) et ces phases dependent d un compteur 
# phases doit etre liste nommee (nom des phases (dans le bon ordre) mais qui correspond pas forcement au nom des stades (c est juste plus simple si on a effectivement une cohorte principale) qui contient listes a 5 ele
  #incrementeur: nom de la fonction qui incremente le compteur
  #parametres: liste des parametres de l incrementeur (liste nommee),
  #seuil: seuil pour le compteur qui declenche chgt de phase
  #matpendant: matrice  de transition a appliquer pendant la phase
  #matchgt: matrice de transition a appliquer le dernier pas de temps de la phase
#rappel: les matrices n ont pas besoin de donner tous les stades, seulement ceux qui sont impliques dans chgts de stades (modification, apparition ou disparition)
#e.c: nom de l espece.condition: sert pour nommer le compteur
#en fait, cree 2 compteurs: celui qui donne la valeur en train d etre incrementee (varicompt) et celui qui donne la liste (nommee) des dates d entree dans la phase (variphase)
#NB: liste des dates de passage allongee par la gauche donc phase actuelle=names(listedates)[1]
#NB: pour especes qui se dispersent, avec evo des stades dependant des conditions du milieu,
#les arrivants dans un pixel gardent leur stade et ne modifient pas la phase de leur condition d arrivee
#attention: le 13/7/2013: enleve le parametre phaseinit: si le compteur n existe pas (pop pas initialisee ou effece par recolte) on ne fait rien donc il FAUT que tous les processus qui peuvent faire apparaitre des pops (invasions, semis) ou qui l enlevent (recolte) enregistrent la date de l action (comme ca, on sait si on est entre recolte et semis (interculture) ou entre semis et recolte (saison culturale)
# par ailleurs, il faut que les interventions de semis aient la possiblilite d initialiser un compteur 

    variphase <- paste("phase", e.c, sep="_")
    varicompt <- paste("compt", e.c, sep="_")

    if(is.null(COMPTEURS[[variphase]])){ 
        return(matrix(1))
    } else {
	# on cherche dans quelle phase du cycle on est
	i <- which(names(phases)==names(COMPTEURS[[variphase]])[1])
	fonction <- phases[[i]]$incrementeur
	arguments <- phases[[i]]$parametres

    # Default values
    if(is.null(fonction)){
        fonction <- "WOSRGDD"
    }
    if(is.null(arguments)){
        arguments <- list()
    }
    if(is.null(phases[[i]]$matpendant)){
        within_phase_mat <- matrix(1)
    }
    if(is.null(phases[[i]]$matchgt)){
        jump_phase_mat <- matrix(1)
    }

	# si fonction a besoin d argument previous, on lui donne
	if(!is.null(formals(fonction)$previous)){
        arguments <- c(arguments, previous = COMPTEURS[[ varicompt ]])
    }
	# on regarde si la phase doit rester la meme (compteur precedent < seuil pour la phase)
	nouveau <- do.call(what = fonction, args = as.list(arguments))

	if(nouveau < phases[[i]]$threshold) # on n est pas encore au seuil de chgt
	{
		#on incremente le compteur
		COMPTEURS[[ varicompt ]] <<- nouveau
        within_phase_mat <- phases[[i]]$matpendant
		return(within_phase_mat)
	} else {
		# on change de phase et on remet le compteur a 0
		COMPTEURS[[ variphase ]] <<- c(list(SIMUTIME),COMPTEURS[[ variphase ]])
        names(COMPTEURS[[ variphase ]])[1] <<- names(phases)[(i %% length(phases)) + 1] 
		COMPTEURS[[ varicompt ]] <<- 0

        jump_phase_mat <- phases[[i]]$matchgt
		return(jump_phase_mat)
	}
}
}
#NB transiphasescompteur est une generalisation de transicompteurlineaire:
# transicompteurlineaire(dureescompteurs=data.frame(durees=c(135,800,80,Inf), incrementeur=c("degreedays","degreedays","days","never"), survienext=c(0.1,0.2,0.3,0), row.names = c("graine", "levee", "rosette", "finflo")),e.c="autre.cas1",condition='TRUE' )
# revient au meme (en plus simple) que
'transiphasescompteur(phases=list(
	germination=list(incrementeur="degreedays", parametres=list( seuil=135, matpendant=matrix(c(1,0,0,0,  0,1,0,0, 0,0,1,0, 0,0,0,1), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo"))), matchgt= matrix(c(0.1,0,0,0,  0,1,0,0, 0,0,1,0, 0,0,0,1), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo")))),
	levee =list(incrementeur="degreedays", parametres=list( seuil=800, matpendant=matrix(c(1,0,0,0,  0,1,0,0, 0,0,1,0, 0,0,0,1), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo"))), matchgt= matrix(c(1,0,0,0,  0,0.2,0,0, 0,0,1,0, 0,0,0,1), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo")))),
	croissance =list(incrementeur="days", parametres=list( seuil=80, matpendant=matrix(c(1,0,0,0,  0,1,0,0, 0,0,1,0, 0,0,0,1), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo"))), matchgt= matrix(c(1,0,0,0,  0,1,0,0, 0,0,0.3,0, 0,0,0,1), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo")))),
	floraison =list(incrementeur="never", parametres=list( seuil=Inf, matpendant=matrix(c(1,0,0,0,  0,1,0,0, 0,0,1,0, 0,0,0,1), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo"))), matchgt= matrix(c(1,0,0,0,  0,1,0,0, 0,0,1,0, 0,0,0,0), ncol=4, dimnames=list(de=c("graine", "levee", "rosette", "finflo"), a=c("graine", "levee", "rosette", "finflo"))))
), e.c="autre.cas1", phaseinit="germination")
'

 



#################### transiperiode
#fonction qui crée matrice de transition en fonction de conditions de sortie pour chaque stade
# une condition  (sur variable globale) de changement par stade (il faut que tous les stades soient donnes et soient dans le bon ordre), 
#un taux de départ par stade (investissement dans le stade suivant)
#un taux de survienext par stade (taux de passage au stade suivant)
#NB pas de mortalité lorsque les stades ne changent pas: la mortalité est prise en compte dans fonction de réponse locale
transiperiode<-function(condchgtstade, investissement, survienext)
{
  #on part de matrice qui change rien (matrice diagonale avec que des 1)
  newmat<-diag(1, nrow=length(names(condchgtstade)), ncol=length(names(condchgtstade)))
  dimnames(newmat)<-list(de=names(condchgtstade), a=names(condchgtstade))
  dep<-which(unlist(lapply(condchgtstade, function(x) eval(parse(text=x)))))
  unchgt<-function(depart)
  {
    arr<-ifelse(depart+1>length(names(condchgtstade)),1,depart+1)
    newmat[depart, arr] <<- survienext[[depart]]
    newmat[depart, depart] <<- 1-investissement[[depart]]
    return()
  }
  lapply(X=dep, FUN=unchgt)
  return(newmat)
}
#exemple: transiperiode(condchgtstade =list(graine= 'mois(SIMUTIME, DEBUTSIMU) %in% 9:11', vegetatif='mois(SIMUTIME, DEBUTSIMU) %in% 5:7', reproducteur='FALSE'), investissement=list(graine= 1, vegetatif=0.2, reproducteur=0), survienext=list(graine= 0.9, vegetatif=0.2, reproducteur=0) )

######fonction utilisables dans dynPEERLESS
equationPEERLESSavecautreressource<-function(N, P, alpha, beta, rn, rp1, rp2, Kn, Kp)
{
  return(
	data.frame(
		N= pmax(N* (1+rn*(1-N/Kn)-alpha*P), 1e-20), 
		P= pmax(P* (1+beta*N-rp1+rp2*(1-P/Kp)), 1e-20),
		predation= alpha*N*P
	) 
 )  
}
equationPEERLESSsansautreressource <-function(N, P, alpha, beta, rn, rp1, Kn)
{
  return(
	data.frame(
		N= pmax(N* (1+rn*(1-N/Kn)-alpha*P), 1e-20), 
		P=pmax(P* (1+beta*N-rp1), 1e-20),
		predation= alpha*N*P
	) 
 )  
}

LotkaVolterra<-function(N, P, alpha, beta, rn, rp1)
{
  return(
	data.frame(
		N= pmax(N* (1 + rn -alpha*P), 1e-20),
		P= pmax(P* (1 + beta*N - rp1), 1e-20),
		predation= alpha*N*P
	) 
 )  
}


######################################définition des fonctions de structure du modele



mortality<-function(...)
{
  #on va chercher les bons parametres
  toto <- PARAMETRES
  names(toto)<-NULL
  noms<-unlist(lapply(toto, function(x) {
	names(x$mortality)
	}), recursive=FALSE)
  if(length(noms)==0) {
      if (OPTIONSIMU$printwhat>2) print("no organism has local mortality")
      return()
  }else{
  if (OPTIONSIMU$printwhat>2) print("local mortality")
  #on calcule toutes les conditions avant de faire des modifs de pops, au cas ou conditions dependraient de pop(s) modifiee(s)

  conditions<-unlist(lapply(toto, function(x) {
                            lapply(x$mortality, function(y) eval(y$parsed_condition))
    }), recursive=FALSE)
  splits<-strsplit(noms, split=".", fixed=TRUE)
  especes<-unlist(lapply(splits,"[[", 1))
  stades<-unlist(lapply(splits,"[[", 2))
  fonctions<-unlist(lapply(toto, function(x) {
	lapply(x$mortality, "[[","fonction")
	}), recursive=FALSE)
  niveaux<-unlist(lapply(especes,function(x) PARAMETRES[[x]]$niv))
  #NB dynunstade a besoin de niveau car utilise forcage sur ses parametres
  parametres<-unlist(lapply(toto, function(x) {
	lapply(x$mortality, "[[","parametres")
	}), recursive=FALSE)

  dynunstade<-function(e,s,cond,l,f,param)
  {
	if (OPTIONSIMU$printwhat>3) print(paste("of", e, s))

    paramRaw <- param[intersect(names(param),names(formals(f)))]
	paramspix<-lapply(paramRaw, 
                      function(x) forcage(tabinit=x, niv=l, cond=cond))
	mortalites<-do.call(f, args= paramspix)
	POPULATIONS[[e]][cond,s]<<-POPULATIONS[[e]][cond,s]*(1-mortalites)
	return()
  }
	#exemple: dynunstade(e="rouille", s="L1", cond=TRUE, l=1, f="sensifroid", param=list(textetemp='changelevel(MILLIEU[["lev2"]][,"temperature", drop=FALSE], nivdep=2, nivarr=1, method="uncountable")[condition,]', tmin=11, topt=15, tletale=1978))
  # temps<-system.time(mapply(FUN= dynunstade, e=especes, s=stades, cond=conditions, l=niveaux, f=fonctions, param=parametres))[2]
  # return(temps)
  mapply(FUN= dynunstade, e=especes, s=stades, cond=conditions, l=niveaux, f=fonctions, param=parametres)
  } #fin if length noms
}

dyntransi<-function(...)
{
  toto<-PARAMETRES
  names(toto)<-NULL
  noms<-unlist(lapply(toto, function(x) {
	names(x$dyntransi)
	}), recursive=FALSE)
  if(length(noms)==0){ 
      if (OPTIONSIMU$printwhat>2) print("no organism has transition dynamics")
      return()
  }else{
  if (OPTIONSIMU$printwhat>2) print("transitions between stages")

  conditions<-unlist(lapply(toto, function(x) {
                            lapply(x$dyntransi, function(y) eval(y$parsed_condition))
    }), recursive=FALSE)

  splits<-strsplit(noms, split=".", fixed=TRUE)
  especes<-unlist(lapply(splits,"[[", 1))
  nomscond<-unlist(lapply(splits,"[[", 2))
  fonctions<-unlist(lapply(toto, function(x) {
	lapply(x$dyntransi, "[[","fonction")
	}), recursive=FALSE)
  parametres<-unlist(lapply(toto, function(x) {
	lapply(x$dyntransi, "[[","parametres")
	}), recursive=FALSE)
  #fonction locale qui fait transition pour une seule espece.condition
  transiuncas<-function(e, cond,f,param, nomcond=""){ 
      if (OPTIONSIMU$printwhat>3){
          print(paste("transiuncas of", e, nomcond))
      }
      matrice<-do.call(f, args=param) #nb: on est obliges de la faire avant de verifier si des pixels verifient 
                    # la condition a cause de transitions basees sur des compteurs: le compteur doit etre 
                    # incremente meme si aucun pixel ne verifie encore la condition
                    # matrice est une matrice qui a rownames=noms des stades de depart, 
                    # colnames=noms des stades d arrivee, parfois que pour les stades qui changent
                    #on verifie qu il y a des pixels qui verifient la condition, sinon on ne fait rien
 	 if (any(cond) ) {
                 if(is.null(matrice)){
                         stop(paste("in dyntransi()/transiuncas(...) for", e, " none of your conditions are verified for ", YYYY, "-", MM, "-", DD, " please check your conditions: ", param[1], sep=""))
                 }
		# on verifie qu il y a bien qqchose qui change 
        # il doit y avoir maniere plus simple de verifier que matrice est pas matrice diagonale avec que des 1)
        if(is.null(matrice)){
            browser()
            stop(paste("In module dyntransi, for function",f,"applied to",e, stade,"none of your conditions:",cond," is verified on the ",YYYY,"-",MM,"-",DD,"verify that for any day one of the conditions is verified"))
        }

		diagonale<-diag(x=1, nrow=dim(matrice)[1], ncol=dim(matrice)[2])
		attributes(diagonale)<-attributes(matrice)
		if (!identical(matrice, diagonale)) {
			if (OPTIONSIMU$printwhat>2){
                print(paste( e, nomcond, "is modified by transiuncas"))
            }
			names(dimnames(matrice))<-NULL
            if(is.null(rownames(matrice))){
                mes <- paste("For",e,"fn:",f,"needs a matrix with stades as column and row names of each matrix passed as phase parameter (matpendant or matchgt)")
                stop(mes)
            } 
	  		biomasse<-POPULATIONS[[e]][cond,rownames(matrice), drop=FALSE]
			POPULATIONS[[e]][cond,colnames(matrice)]<<-biomasse %*% matrice
		  }
	  }
	  return()
  }
  # browser(expr=SIMUTIME==166)
  # temps<-system.time(mapply(FUN= transiuncas, e=especes, cond=conditions, f=fonctions, param=parametres, nomcond= nomscond))[2]
  mapply(FUN= transiuncas, e=especes, cond=conditions, f=fonctions, param=parametres, nomcond= nomscond)
  # return(temps)
  } #fin if length noms
}

invasion<-function()
{
    indice<-unlist(lapply(INVASIONS, function(i) (i$wrap & i$date==convertit2() | !i$wrap & i$date==SIMUTIME)))
    if (OPTIONSIMU$printwhat>2) print("invasion")
    localFun <- function(e, s, f, p, compt) {
		if (OPTIONSIMU$printwhat>3) print(paste(e,s, sep="."))
		POPULATIONS[[e]][,s]<<-do.call(what=f, args=p)
		lapply(names(compt), function(n) {
    			COMPTEURS[[paste("phase", n, sep="_")]]<<-SIMUTIME
    			names(COMPTEURS[[paste("phase", n, sep="_")]])[1]<<-compt[[n]]
    			COMPTEURS[[paste("compt", n, sep="_")]]<<-0
    			return()
   		 })
		return()
	}
    mapply(localFun,
	e=lapply(INVASIONS[indice], "[[", "esp"), 
	s=lapply(INVASIONS[indice], "[[", "sta"), 
	compt=lapply(INVASIONS[indice], "[[", "compteurscrees"),
	f=lapply(INVASIONS[indice], "[[", "fonction"), 
	p=lapply(INVASIONS[indice], "[[", "parametres")
  )
}



'
# a enlever car finalement pas generique:
#################### transicompteurlineaire
transicompteurlineaire<-function(dureescompteurs, e.c, condition)
{
#fonction qui crée matrice de transition en fonction de duree de chaque stade (du premier au dernier mais PAS BOUCLE, et 100 pourcent du stade le quitte au moment du changement de stade
# et qui incremente un compteur global pour toute la condition
#NB: compteur créé par la fonction s il n existe pas encore mais que la pop existe (apportee par semis), initialise au premier stade
#NB: compteur effacé lorsque la pop du stade actuel disparait, (par recolte ou par predation totale): dans cas ou recolte fait tomber les graines, le compteur est automatiquement recree tout de suite apres
#comme croissance colza dans mosaic-pest
#dureescompteur doit etre data.frame avec row.names=nom des stades (dans le bon ordre) qui contient 
  #durees du stade (en unites du compteur OU seuil de declenchement si incrementeur a un argument nomme threshold), 
  #incrementeur: nom de la fonction qui incremente le compteur
  #survienext: taux de survie vers le stade suivant
#e.c: nom de l espece.condition: sert pour nommer le compteur et pour verifier si l espece a ete semee
#condition: condition (format texte) de e.c car on en a besoin pour verifier si l espece a ete semee dans les pixels ou la condition est vraie
#attention: une seule cohorte "officielle" pour toute la condition (p exple variete) car un seul compteur
#donc si il y a repousses, leur croissance commence au moment ou toute la population officielle est detruite (par recolte, mais attention: destruction totale de la culture par predation cause aussi declenchement des repousses).
#en fait, cree 2 compteurs: celui qui donne la valeur en train d etre incrementee (varicompt) et celui qui donne la liste (nommee) des dates d entree dans la phase (variphase)
#NB: liste des dates de passage allongee par la gauche donc stade actuel=names(listedates)[1]
#attention: pour especes qui se dispersent, avec evo des stades dependant des conditions du milieu,
#les arrivants dans un pixel gardent leur stade et ne modifient pas la phase de leur condition d arrivee
#donc a eviter: on perd coherence 1 phase = 1 stade, sauf si resynchronisation des phases de toutes les conditions avant dispersion
  e<-unlist(lapply(strsplit(e.c, split=".", fixed= TRUE), "[[", 1))
  #on va suivre deux compteurs: celui qui donne la valeur en train d etre incrementee (varicompt) et celui qui donne la liste (nommee) des dates d entree dans le phase (variphase)
  variphase<-paste("phase", e.c, sep="_") 
  varicompt<-paste("compt", e.c, sep="_")
  # on trouve ou on en est
  phase<-names(COMPTEURS[[variphase]])[1]
  i<-which(row.names(dureescompteurs)==phase)
  #si le compteur existe mais que la pop a ete detruite entierement (par exemple par recolte, ou par predation totale ou autre), on efface le compteur
  if (! is.null(COMPTEURS[[variphase]]) && sum(POPULATIONS[[e]][eval(parse(text=condition)), phase ])==0 )
  { 
	COMPTEURS[[variphase]]<<-NULL
	COMPTEURS[[varicompt]]<<-NULL
  } #NB: ici, on ne met pas de else car si recolte a laisse des graines, il faut avoir efface le compteur par commande precedente puis directement le recreer pour calculer la dormance eventuelle des graines
  #si le compteur n existe pas et que la pop en premier stade existe (soit par semis, soit par grainesquitombent pendant recolte), on cree les compteurs et on met le compteur a 0
  if (is.null(COMPTEURS[[variphase]]) && sum(POPULATIONS[[e]][eval(parse(text=condition)), row.names(dureescompteurs)[ 1 ] ])>0 )
  { 
	toto<-list(SIMUTIME); names(toto)<-row.names(dureescompteurs)[ 1 ]
	COMPTEURS[[variphase]]<<-toto
	COMPTEURS[[varicompt]]<<-0
	phase<-row.names(dureescompteurs)[ 1 ]
	i<-1
  } 
  if (is.null(COMPTEURS[[variphase]])) return(diag(1)) else #s il n y a plus de pop, on renvoie matrice identite, pour que dyntransi fasse rien sinon on fait les calculs necessaires
  { 
	fonction<-dureescompteurs[i, "incrementeur"]
	arguments<-formals(fonction)
	#si fonction a besoin d argument previous ou threshold, on lui donne
	if (!is.null(arguments$previous)) arguments$previous<-COMPTEURS[[ varicompt ]]
	if (!is.null(arguments$threshold)) arguments$threshold <-dureescompteurs[i, "durees"]
	if (COMPTEURS[[ varicompt ]] < dureescompteurs[i, "durees"] ) { #on n est pas encore au seuil de chgt
		COMPTEURS[[ varicompt ]]<<-  do.call(what=fonction, args=as.list(arguments))
		return(matrix(1, ncol=1, dimnames=list(phase, phase)))
 	 } else { #on a passe le seuil de chgt
		toto<-list(SIMUTIME); names(toto)<-row.names(dureescompteurs)[i+1]
		COMPTEURS[[ varicompt ]]<<- 0
		COMPTEURS[[variphase]]<<-c(toto, COMPTEURS[[variphase]])
		return(matrix(c(0,0,1,0), ncol=2, dimnames=list(de=c(phase,row.names(dureescompteurs)[i+1]), a=c(phase, row.names(dureescompteurs)[i+1]))))
	  }
	
  }
}
'
#exemple transicompteurlineaire(dureescompteurs=data.frame(durees=c(135,800,80, 400, 300, 900, 	Inf), incrementeur=c("degreedays","degreedays","days","degreedays","degreedays","degreedays","never"), survienext=c(1,1,1,1,1,1,0), row.names = c("graine", "levee", "rosette", "reprise", "debutflo", "flo", "finflo")),e.c="colza.varnormale",condition='MILIEU[["lev1"]]$variete=="normale"' )

