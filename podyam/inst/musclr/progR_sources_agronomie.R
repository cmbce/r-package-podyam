##attention: convention necessaire pour que les fonction d agronomie marchent:
##dans MILIEU, typecult contient info cult_SdC (avant l'implantation de la culture et pendant la culture)
##             variete contient "rien" avant l'implantation de la culture et le nom de la ou des variétés après l'implantation NON: remplace par darterecolte et datesemis
##		daterecolte (si daterecolte>datesemis = interculture; si daterecolte<=datesemis = saison culturale
##		datesemis

##attention= pas logique: fonction utilisables dans farming font forcage elle-memes alors que fonction utilisables dans updateenvt recoivent leur parametres deja forces (forcage par updetenvt)
#mais obligatoire car fonction agronomiques ont souvent parametres non forcables, et jonglent plus entre les niveaux (niveau de la decision, niveau de la pop, niveau de l enregistrement des actions...


################ sous-fonctions collaterales qui peuvent etre appelees par les fonctions de pratiques culturales


collateral<-function(qui, combien, nivpix, pix)
{ 
#destruction d'UNE (esp.sta) victime collaterale des interventions techniques
#si espece pas definie au meme niveau que pix, mortalite proportionnelle au nombre de pixels impactes

    splits<-strsplit(qui, split=".", fixed=TRUE)
    esp<-unlist(lapply(splits, "[[", 1))
    sta<-unlist(lapply(splits, "[[", 2))
    if(esp %in% names(PARAMETRES)) { if (sta %in% PARAMETRES[[esp]]$stades) {
    nivpop<-PARAMETRES[[esp]]$niv
    mortalites<-forcage(tabinit=combien, niv= nivpop)
    propmodif<-changelevel(data.frame(nbpix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep= nivpix, nivarr=nivpop, method="uncountable")$nbpix
    mortalites<-mortalites* as.numeric(propmodif)
	#il suffit d avoir au moins un pixagro modifie pour que le pixpop soit  recalcule
    POPULATIONS[[esp]][as.logical(propmodif) , sta]<<-POPULATIONS[[esp]][as.logical(propmodif) , sta]*(1- mortalites[as.logical(propmodif)]) 
  }}
  return(invisible())
}

chamboule<-function(esp, matrice, nivpix, pix)
{ 
#transitions de stades (ou de couches du sol) d'UNE (esp) suite a une intervention technique
#matrice doit avoir comme nom de ligne et de colonne les noms des stades de depart et d arrivee
#si espece pas definie au meme niveau que les interventions techniques, il suffit qu un pixel nivpix soit modifie pour que tout le pixel nivpop soit chamboule
    if(esp %in% names(PARAMETRES)) { 
    names(dimnames(matrice))<-NULL
    pixpop<-as.logical(changelevel(data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep= nivpix, nivarr=PARAMETRES[[esp]]$niv, method="uncountable")$pix)
    biomasse<-POPULATIONS[[esp]][pixpop,rownames(matrice), drop=FALSE]
    POPULATIONS[[esp]][pixpop,colnames(matrice)]<<-biomasse %*% matrice
  }
  return()
}


################# fonctions au choix pour pratiques culturales
### attention: si on en ajoute d autres, il faut decider dans init.txt (qui) dans quel statut de parcelle (repere par daterecolte avant ou apres datesemis) il s applique: avant semis ou apres semis ou qqsoit le statut de la parcelle (important pour gestion des juxtapositions des cultures en debut et fin de cycle dans differentes parcelles)
## par defaut, une action s applique dans tous les statuts de parcelles
#pour l instant "desherbagemecanique", "retournementsol", "semis", "semisassoc", "changeanneesolnu" s appliquent avant semis
# et "recolte", "recolteassoc", "changeannee", "traitement" s appliquent apres semis

#la fonction farming passe les pix a modifier au niveau 1, charge aux fonctions des pratiques individuelles de modifier les pops et le milieux au niveau voulu et d enregistrer les infos au niveau voulu (nivdecision, qui est commun a toutes les pratiques)

desherbagemecanique<-function(tueaussi=NULL, inverseaussi=NULL, enregistredate=FALSE, niv, pix)
{ 
#desherbagemecanique modifie POPULATIONS (peut tuer certains organismes du sol, détruire l'apareil végétatif et tuer une partie des graines des mh, deplacer verticalement les residus infectes...
##pourrait modifier MILIEU aussi, mais pour l'instant, il n'y a rien dans milieu qui pourrait être modifié par labour
#tueaussi= list nommee par esp.stade des taux de mortalite dus au passage de l outil (les mortalites peuvent dependre du milieu via forcage (cf fonction collateral))
#inverseaussi= liste nommee par espece des matrices de transfert d un stade a un autre (matrice carree dont rownames=noms des stades de depart, colnames= noms des stades d arrivee): pas forcable
#NB: pix (boolean) est donne au niveau 1
#niv: niv auquel l outil est applique (en fait, pas utilise: les pops sont impactees a leur niveau, la date est enregistree a nivdecision
    #tue victimes collatérales
    mapply(collateral, qui=names(tueaussi), combien=tueaussi, MoreArgs=(list(pix=pix, nivpix ="1")))
    #inverse les couches de sol
    mapply(chamboule, esp=names(inverseaussi), matrice=inverseaussi, MoreArgs=(list(pix=pix, nivpix="1")))
    pixdecision<-as.logical(changelevel(data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep="1", nivarr=INTERVENTIONS$nivdecision, method="uncountable")$pix)
    if (enregistredate) MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "datetravail"]<<-SIMUTIME
  return()
}
#exemple:
'
desherbagemecanique(tueaussi=list(ble.vegetatif=100), inverseaussi=list(phoma=matrix(c(0,1,1,0), nrow=2, dimnames=list(c("res1", "res2"), c("res1", "res2")))), pix=TRUE, niv=1)
'

#on cree une copie de desherbagemecanique pour travailsol: font la meme chose mais travailsol s applique au parcelles pas encore semees alors que desherbagemecanique s applique aux parcelles semees
travailsol<-desherbagemecanique


retournementsol<-function(outil, popimpactees, enregistredate=FALSE, niv, pix)
{
#retournementsol est moins generique que outil: matrice de retournement sont codees "en dur" dans la fonction (d apres SIPPOM)
#mais plus facile a entrer dans parametres: toutes les pops sont impactees de la meme facon (marche que si ont memes decoupages en horizons)
# modifie POPULATIONS (deplace verticalement les residus infectes)
#outil = nom de l outil dont on a la matrice de retournement dans SIPPOM
#popimpactee=liste des especes impactees, avec les stades qui correspondent à chacun des horizons de sol :exple popimpactees=list(phoma=c("res1", "res2", "res3", "res4"))
#NB: pix (boolean) est donne au niveau 1
#niv: niv auquel l outil est applique (en fait, pas utilise: les pops sont impactees a leur niveau, la date est enregistree a nivdecision
  #matrices avec en lignes les stades de depart et en colonnes les stades d arrivee (dimnames=list(de=paste("horiz", 1:4), a=paste("horiz", 1:4)))
  Labour=matrix(c(c(0, 0.05, 0.417, 0.1), c(0.017, 0.3, 0.333, 0.05), c(0.8, 0.517, 0.117, 0.05), c(0.183, 0.133, 0.133, 0.8)),  nrow=4)
  CoverCrop=matrix(c(c(0.361, 0.14, 0, 0), c(0.518, 0.392, 0, 0), c(0.121, 0.468, 0.7, 0), c(0, 0, 0.3, 1)),  nrow=4)
  Chisel=matrix(c(c(0.306, 0.163, 0, 0), c(0.623, 0.475, 0.017, 0), c(0.051, 0.345, 0.715, 0), c(0.02, 0.017, 0.268, 1)),  nrow=4)
  Herse=matrix(c(c(0.779, 0.449, 0, 0), c(0.212, 0.360, 0.019, 0), c(0.009, 0.170, 0.787, 0), c(0, 0.021, 0.194, 1)),  nrow=4)
  Semoir=matrix(c(c(0.957, 0.2, 0, 0), c(0.043, 0.8, 0.017, 0), c(0, 0, 0.983, 0.017), c(0, 0, 0, 0.983)),  nrow=4)
  if (!exists(outil, inherit=FALSE)) stop(paste("il n'y a pas de matrice de retournement correspondant à l' outil", outil, "dans la fonction retournementsol (attention aux majuscules)"))
  #renomme les lignes et colonnes de la bonne matrice pour qu elles correspondent a chacune des pops impactees
  matrices<-lapply(popimpactees, function(stades) {mat<-get(outil); rownames(mat)<-stades; colnames(mat)<-stades; return(mat)})
  mapply(chamboule, esp=names(popimpactees), matrice= matrices, MoreArgs=(list(pix=pix, nivpix="1")))
  pixdecision<-as.logical(changelevel(data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep="1", nivarr=INTERVENTIONS$nivdecision, method="uncountable")$pix)
    if (enregistredate) MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "datetravail"]<<-SIMUTIME
  return()
}
#exemple:
'
retournementsol(outil= "Labour", popimpactees=list(phoma=c("res1", "res2", "res3", "res4")), pix=TRUE, niv=1)

'


semis<-function(espece, variete, densitekgha, compteurscrees=NULL, niv, pix)
{   
#semis modifie MILIEU (variété et sowing density au niveau de la pop, datesemis au niveau de nivdecision) et POPULATIONS (remplace pop éventuellement existante par qté apportée) 
#(on peut faire du semis sous couvert, mais que si espèce différente)
#sowingdensity est somme des densites des differents stades semes
#niv: niv auquel l outil est applique (sert pour faire forcage des parametres)
#nb: pix est donne au niveau 1
#NB: semis enregistre forcement date, mais enregistre sowingdensity et variete que si existent deja dans le milieu (au niveau de la pop)

    varietes<-data.frame(variete=forcage(tabinit=variete, niv=niv), row.names=rownames(ESPACE[[paste("lev", niv, sep="")]]))

    densites<-lapply(densitekgha, forcage, niv=niv)
    densites <-as.data.frame(densites)
    rownames(densites)<-rownames(ESPACE[[paste("lev", niv, sep="")]])
    densitetousstades<-rowSums(densites)

    pixdecision<-as.logical(changelevel(
	data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), 
	nivdep="1", nivarr=INTERVENTIONS$nivdecision, method="uncountable"
    )$pix) # si au moins 1 pix de nivdecision est modifie, on enregistre infos
    nivespece<-PARAMETRES[[espece]]$niv
    pixnivespece<-as.logical(as.numeric(changelevel(
	data.frame(nbpix=pix, row.names=rownames(MILIEU$lev1)), 
	nivdep="1", nivarr= nivespece, method="mostfrequent"
    )$nbpix)) # si la majorite des pix de nivpop est concernee, on seme la plante

    if ("variete" %in% colnames(MILIEU[[paste("lev", nivespece,sep="")]]))
    {
    		MILIEU[[paste("lev", nivespece,sep="")]][pixnivespece , "variete"] <<- (
		changelevel(varietes, nivdep=niv, nivarr=nivespece, method="mostfrequent")[pixnivespece,]
    		)
    }
   if ("sowingdensity" %in% colnames(MILIEU[[paste("lev", nivespece,sep="")]]))
    {
   	 MILIEU[[paste("lev", INTERVENTIONS$nivdecision,sep="")]][pixdecision , "sowingdensity"] <<- (
		changelevel(densitetousstades, nivdep=niv, nivarr=INTERVENTIONS$nivdecision, method="uncountable")[pixdecision]
    	)
   }

    densitesnivespece<-changelevel(densites, nivdep=niv, nivarr=nivespece, method="uncountable")
    if (suppressWarnings(is.na(as.numeric(nivespece)))) {
	surf<-GRAIN^2*ESPACE[[paste("lev", nivespece, sep="")]]$nbpixlev1  
    } else {
	surf <-(GRAIN*TC^(as.numeric(nivespece)-1))^2
    }
    POPULATIONS[[espece]][pixnivespece , names(densites)] <<- as.numeric(
		densitesnivespece[pixnivespece, names(densites)]
    )*surf/10 #(densites en kg/ha, biomasse en g par pixel)
    
    MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "datesemis"]<<-SIMUTIME
    lapply(names(compteurscrees), function(n) {
	variphase<-paste("phase", n, sep="_")
	varicompt<-paste("compt", n, sep="_")
	if (
		is.null(COMPTEURS[[variphase]])
		| COMPTEURS[[variphase]] [[1]] != SIMUTIME 
		| names(COMPTEURS[[variphase]][1]) != compteurscrees[[n]]
	) { #on modifie compteur que si ne vient pas juste d etre cree par pratique dans autre SdC qui partagerait meme compteur
    		COMPTEURS[[paste("phase", n, sep="_")]]<<-c(list(SIMUTIME), COMPTEURS[[paste("phase", n, sep="_")]])
    		names(COMPTEURS[[paste("phase", n, sep="_")]])[1]<<-compteurscrees[[n]]
    		COMPTEURS[[varicompt]]<<-0
	}
    	return()
    }) #NB: si le compteur n a pas ete efface par recolte, on continue a l incrementer, sinon on le cree
  return()
}
#exemple:
'
semis(espece="ble", variete="blabla", densitekgha =list(graine=100, vegetatif=0, reproducteur=0), niv=1, pix=MILIEU[["lev1"]]$variete =="rien" & MILIEU[["lev1"]]$typeculture =="ble1_bio")
semis(espece="colza", variete="normale", densitekgha =list(graine=100), niv=1, pix=MILIEU[["lev1"]]$variete =="rien" & MILIEU[["lev1"]]$typeculture =="colza1_conv")

'

semisassoc<-function(espece, variete, densitekgha, compteurscrees=NULL, niv, pix)
{    
#NB: TODO semis enregistre forcement date
#NB: TODO semis enregistre forcement date, mais enregistre sowingdensity et variete que si existent deja dans le milieu

  stop("pas encore corrige semisassoce depuis que pratiques peuvent etre a niveau autre que 1")

    varietes<-forcage(tabinit=variete, niv=niv)
    MILIEU[[paste("lev", niv,sep="")]][pix , "variete"] <<- varietes[pix]
    especes<-unlist(strsplit(espece, ".", fixed=TRUE))
    stades<-names(densitekgha)
    #manipe un peu compliquée pour appliquer forcage à densités 
    #qui est liste de stades chacun potentiellement dépendant du milieu 
    #et chaque valeur étant composée des différentes espèces associées
    #on obtient une liste avec un élé par espèce, chaque élé=une liste (un élé par stade), chaque élé=vecteur des valeurs par pixel 
    toto<-lapply(densitekgha, forcage, niv=niv)
    MILIEU[[paste("lev", niv,sep="")]][pix , "sowingdensity"] <<- rowSums(  as.data.frame(lapply(toto, function(x) unlist(lapply(strsplit(x, split="_", fixed=TRUE), function(c) sum(as.numeric(c))))) )[pix,,drop=FALSE]  )
	#sowingdensity est somme des densites des differents stades des differentes especes semees
    getesp<-function(numesp)
    {
      delistsplit<-function(x, numesp)
      {
        tutu<-strsplit(x, split="_", fixed=TRUE)
        sapply(tutu, "[[", numesp)
      }
      titi<-lapply(toto, delistsplit, numesp=numesp)
    }
    densites<-lapply(1:length(especes), getesp)
    semunespece<-function(e,d)
    {
      semunstade<-function(e, s, d)
      { 
        POPULATIONS[[e]][pix , s] <<- as.numeric(d[pix])*(GRAIN*TC^as.numeric(niv)-1)^2/10 #(densites en kg/ha, biomasse en g par pixel)
        return()
      }
      mapply(semunstade, d=d, s=names(d), MoreArgs=list(e=e))
      return()
    } 
    mapply(semunespece, e=especes, d=densites)
    if(enregistredate) MILIEU[[paste("lev", niv,sep="")]][pix , "datesemis"]<<-SIMUTIME
    lapply(names(compteurscrees), function(n) {
    	COMPTEURS[[paste("phase", n, sep="_")]]<<-SIMUTIME
    	names(COMPTEURS[[paste("phase", n, sep="_")]])[1]<<-compteurscrees[[n]]
    	COMPTEURS[[paste("compt", n, sep="_")]]<<-0
    	return()
    })
  return()
}
#exemple:
'
POPULATIONS<-c(POPULATIONS, list(trefle=data.frame(graine=numeric(dim(ESPACE[["lev1"]])[1]), rosette=numeric(dim(ESPACE[["lev1"]])[1]) )))
semisassoc(espece="colza.trefle", variete="precoce.alexandrie", densitekgha =list(graine="100_50", rosette="10_0"), niv=1, pix=TRUE)
'




recolte<-function(cult, SdC, espece, intensite, grainesquitombent=NULL, tueaussi=NULL, changeannee=TRUE, enregistrepreced=FALSE, compteurseffaces=NULL, niv, pix)
{
#recolte modifie MILIEU (variété au niveau de la pop (si elle existe), typeculture (culturesuivante) au niveau 1, preced, daterecolte au niveau nivdecision) et POPULATIONS (récolte la plante et peut faire tomber une partie des graines et tue les victimes collatérales)
#modifie pas sowingdensity donc sowingdensity reste identique jusqu a semis culture suivante
#si changeannee alors enregistre la date (obligatoire maintenant que c est les dates qui determinent la saison culturale
#attention: penser a mettre changeannee=FALSE pour une fauche en cours de culture
#TODO: remplacer grainesqui tombent (du type ble=0.4) par volunteers (du type ble.graine=0.4)

    intensites<-lapply(intensite, forcage, niv=niv)
    pixdecision<-as.logical(changelevel(data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep="1", nivarr=INTERVENTIONS$nivdecision, method="uncountable")$pix) # si au moins 1 pix de nivdecision est modifie, on enregistre infos

    nivespece<-PARAMETRES[[espece]]$niv
    pixnivespece<-as.logical(as.numeric(changelevel(data.frame(nbpix=pix, row.names=rownames(MILIEU$lev1)), nivdep="1", nivarr= nivespece, method="mostfrequent")$nbpix)) # si la majorite des pix de nivpop est concernee, on seme la plante

    #tue victimes collatérales
    nomstues<-names(tueaussi)
    mapply(collateral, qui=nomstues, combien=tueaussi, MoreArgs=(list(pix=pix, nivpix="1")))
    #enleve variete et change typeculture en la culture suivante si changeannee=TRUE 
    if (changeannee)
    { 
     if (enregistrepreced) MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "precedent"]<<-cult
    if ("variete" %in% colnames(MILIEU[[paste("lev", nivespece,sep="")]])) MILIEU[[paste("lev", nivespece,sep="")]][pixnivespece , "variete"]<<-"rien"
     numnext<-which(ROTATIONS[[SdC]]==cult)+1
     if (numnext>length(ROTATIONS[[SdC]])) numnext<-1
     if (OPTIONSIMU$printwhat>1) print(paste(cult, SdC,"devient",paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")) ) 
     MILIEU[['lev1']][pix , "typeculture"]<<-paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")
     MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "daterecolte"]<<-SIMUTIME
    }
    #retire les biomasses récoltées
    intensites<-as.data.frame(intensites)
    rownames(intensites)<-rownames(ESPACE[[paste("lev", niv, sep="")]])
    intensitesnivespece<-changelevel(as.data.frame(intensites), nivdep=niv, nivarr=nivespece, method="uncountable", na.rm=TRUE)


    POPULATIONS[[espece]][pixnivespece , names(intensites)] <<- POPULATIONS[[espece]][pixnivespece , names(intensites)] * (1-as.matrix(
		intensitesnivespece[pixnivespece, names(intensites)]
    ))

     #seme les graines qui tombent
    if (!is.null(grainesquitombent)) 
    { 
      graines<-forcage(tabinit=grainesquitombent, niv= nivespece)
      POPULATIONS[[espece]][pixnivespece , "graine"]<<- POPULATIONS[[espece]][pixnivespece , "graine"] + graines
    }
    #efface les compteurs demandes
    lapply(compteurseffaces, function(e.c) {COMPTEURS[[paste("phase", e.c, sep="_")]]<<-NULL; COMPTEURS[[paste("compt", e.c, sep="_")]]<<-NULL; return()})
  return()
}

### tres lent, voir comment accélérer
recolteassoc<-function(cult, SdC, espece, intensite, grainesquitombent=NULL, tueaussi=NULL, changeannee=TRUE, enregistrepreced=FALSE, compteurseffaces=NULL, niv=1, pix)
{   
#recolte modifie MILIEU (variété, et typeculture (culturesuivante)) et POPULATIONS (récolte la plante et peut faire tomber une partie des graines et tue les victimes collatérales)
#modifie pas sowingdensity donc sowingdensity reste identique jusqu a semis culture suivante
  stop("pas encore corrige recolteassoc depuis que pratiques peuvent etre a niveau autre que 1")

    #tue victimes collatérales
    nomstues<-names(tueaussi)
    mapply(collateral, qui=nomstues, combien=tueaussi, MoreArgs=(list(pix= pix, nivagro=niv)))
    #enleve variete et change typeculture en la culture suivante si la récolte est totale
    if (changeannee) 
    {
       if (enregistrepreced) MILIEU[[paste("lev", niv,sep="")]][pix , "precedent"]<<-cult
           if ("variete" %in% colnames(MILIEU[[paste("lev", nivespece,sep="")]])) MILIEU[[paste("lev", niv,sep="")]][pix , "variete"]<<-"rien"
       numnext<-which(ROTATIONS[[SdC]]==cult)+1
       if (numnext>length(ROTATIONS[[SdC]])) numnext<-1
       if (OPTIONSIMU$printwhat>1) print(paste(cult, SdC,"devient",paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")) ) 
	MILIEU[[paste("lev", niv,sep="")]][pix , "daterecolte"]<<-SIMUTIME
       MILIEU[[paste("lev", niv,sep="")]][pix , "typeculture"]<<-paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")
    }
    #retire les biomasses récoltées
    especes<-unlist(strsplit(espece, ".", fixed=TRUE))
    stades<-names(intensite)
    #manipe un peu compliquée pour appliquer forcage à densités 
    #qui est liste de stades chacun potentiellement dépendant du milieu 
    #et chaque valeur étant composée des différentes espèces associées
    #on obtient une liste avec un élé par espèce, chaque élé=une liste (un élé par stade), chaque élé=vecteur des valeurs par pixel 
    toto<-lapply(intensite, forcage, niv=niv)
    getesp<-function(numesp)
    {
      delistsplit<-function(x, numesp)
      {
        tutu<-strsplit(x, split="_", fixed=TRUE)
        sapply(tutu, "[[", numesp)
      }
      titi<-lapply(toto, delistsplit, numesp=numesp)
    }
    intensites<-lapply(1:length(especes), getesp)
    recunespece<-function(e,d)
    {
      recunstade<-function(e, s, d)
      {
        POPULATIONS[[e]][pix , s]<<-POPULATIONS[[e]][pix , s]*(1- as.numeric(d[pix])) 
        return()
      }
      mapply(recunstade, d=d, s=names(d), MoreArgs=list(e=e))
      return()
    } 
    mapply(recunespece, e=especes, d=intensites)
    #idem pour les graines qui tombent
    if (!is.null(grainesquitombent)) 
    { 
      toto<-forcage(tabinit=grainesquitombent, niv=niv)
      graines<-lapply(1:length(especes), getesp)
      grainunesp<-function(e, g)
      {
          POPULATIONS[[e]][pix , "graine"]<<- POPULATIONS[[e]][pix , "graine"] + as.numeric(g[pix])
      }
      mapply(grainunesp, e=especes, g=graines)
    }
    #efface les compteurs demandes
    lapply(compteurseffaces, function(e.c) {COMPTEURS[[paste("phase", e.c, sep="_")]]<<-NULL; COMPTEURS[[paste("compt", e.c, sep="_")]]<<-NULL; return()})
  return()
}

changeannee<-function(cult, SdC, enregistrepreced=FALSE, niv, pix)
{   
#changeannee modifie MILIEU (typeculture (culturesuivante) au niveau 1, preced, daterecolte au niveau nivdecision)
	pixdecision<-as.logical(changelevel(data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep="1", nivarr=INTERVENTIONS$nivdecision, method="uncountable")$pix) # si au moins 1 pix de nivdecision est modifie, on enregistre infos
    
	if (enregistrepreced) MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "precedent"]<<-cult
	MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "daterecolte"]<<-SIMUTIME
	numnext<-which(ROTATIONS[[SdC]]==cult)+1
	if (numnext>length(ROTATIONS[[SdC]])) numnext<-1
	if (OPTIONSIMU$printwhat>1) print(paste(cult, SdC,"devient",paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")) ) 
	MILIEU[['lev1']][pix , "typeculture"]<<-paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")
  return()
}

changeanneesolnu<-function(cult, SdC, enregistrepreced=FALSE, niv, pix)
{   
#idem que changeannee, mais nom different car quand on reformate itineraires dans init, on a besoin de distinguer les deux: si on ne declarait rien, on ne pourrait pas faire sol nu pendant plus de 11 mois (semis de culture suivante aurait lieu tout de suite, donc il faut declarer sol nu mais comme changeannee n est appliquee que dans pixels semes et pas encore recoltes , il faut une autre facon de changer l annee. donc quand sol nu pendant plus de 12 mois: il faut le déclarer comme culture ("solnu" ou n importe quelle autre culture qui n est jamais semee) dans la rotation (avec comme itineraire technique changeanneesolnu donnant une date apres la recolte de la culture precedente et avant implantation de la culture suivante (et eventuellement des interventions de travail du sol)
	pixdecision<-as.logical(changelevel(data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep="1", nivarr=INTERVENTIONS$nivdecision, method="uncountable")$pix) # si au moins 1 pix de nivdecision est modifie, on enregistre infos
	if (enregistrepreced) MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "precedent"]<<-cult
	MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "daterecolte"]<<-SIMUTIME
	numnext<-which(ROTATIONS[[SdC]]==cult)+1
	if (numnext>length(ROTATIONS[[SdC]])) numnext<-1
	if (OPTIONSIMU$printwhat>1) print(paste(cult, SdC,"devient",paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")) ) 
	MILIEU[['lev1']][pix , "typeculture"]<<-paste(ROTATIONS[[SdC]][numnext],SdC, sep="_")
  return()
}

traitement<-function(produit, dose, enregistredate=FALSE, niv, pix)
{
#traitement modifie MILIEU (produit)
    pixdecision<-as.logical(changelevel(data.frame(pix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep="1", nivarr=INTERVENTIONS$nivdecision, method="uncountable")$pix) # si au moins 1 pix de nivdecision est modifie, on enregistre infos
    if(enregistredate) MILIEU[[paste("lev", INTERVENTIONS$nivdecision, sep="")]][pixdecision , "datetraitement"]<<-SIMUTIME
    
    pixmodifs<-as.logical(changelevel(data.frame(nbpix=as.numeric(pix), row.names=rownames(MILIEU$lev1)), nivdep= '1', nivarr=niv, method="uncountable")$nbpix)
    doses<-forcage(tabinit=dose, niv=niv)
    MILIEU[[paste("lev", niv,sep="")]][pixmodifs , produit]<<-doses[pixmodifs]
  return()
}


################# fonctions au choix pour actualise milieu

setwater<-function(RU, niv, pix=TRUE)
{
  eaupluie<-pluie() * (GRAIN*TC^(as.numeric(niv)-1))^2 * 1000  #scenario donne des mm, on veut des g dans le pixel surface d un pixel = GRAIN*nb de pixels de niveau 1 dans le pixel de nivau niv
  MILIEU[[paste("lev",niv,sep="")]][pix,"eau"] <<- pmin(MILIEU[[paste("lev",niv,sep="")]][pix,"eau"]+ eaupluie, RU)
  return(invisible())
}

set_temp_minimaximean<-function(diffmini, diffmaxi, niv, pix=TRUE)
{
  temp<-temperature()
  MILIEU[[paste("lev",niv,sep="")]][pix,"tmoyloc"] <<- temp-diffmini
  MILIEU[[paste("lev",niv,sep="")]][pix,"tminloc"] <<- temp
  MILIEU[[paste("lev",niv,sep="")]][pix,"tmaxloc"] <<- temp+diffmaxi
  return(invisible())
}

setlight<-function(niv, pix=TRUE)
{
  lum<-lumiere()*(GRAIN*TC^(as.numeric(niv)-1))^2 #scenario donne des Joules par m2, on veut des Joules dans un pixel de surface = GRAIN*nb de pixels de niveau 1 dans le pixel de nivau niv
  MILIEU[[paste("lev",niv,sep="")]][pix,"lumiere"] <<- lum
  return(invisible())
}

declin<-function(nom, tauxdec, niv, pix=TRUE)
{
  nom<-nom[1] #car nom est passe par forcage
  pix<-pix & MILIEU[[paste("lev",niv,sep="")]][, nom]>0 & tauxdec>0
  MILIEU[[paste("lev",niv,sep="")]][pix, nom] <<- MILIEU[[paste("lev",niv,sep="")]][pix, nom]*(1-tauxdec[pix])
  return(invisible())
}

reset<-function(nom, valeur, niv, pix=TRUE)
{
  nom<-nom[1] #car nom est passe par forcage
  MILIEU[[paste("lev",niv,sep="")]][pix, nom] <<- valeur
  return(invisible())
}


###################### fonctions de structure

updateenvt<-function(...)
{
  if (OPTIONSIMU$printwhat>2) print("update environment")
  fonctions<-lapply(names(PARAMACTUALISE), get, envir=ICI)
  parametres<-lapply(PARAMACTUALISE, function(x) {
	if(is.null(x$pix)) pix<-TRUE else pix<-x$pix
	return(c(list(niv=x$niv, pix=pix), lapply(x[setdiff(names(x), c("niv", "pix"))], forcage, niv=x$niv, cond=pix)) )
  })
  # temps<-system.time(mapply(do.call, what=fonctions, args= parametres))[2]
  # return(temps)
  mapply(do.call, what=fonctions, args= parametres)
}

updateenvt2<-function()
{
  if (OPTIONSIMU$printwhat>2) print("update environment")
  fonctions<-lapply(names(PARAMACTUALISE2), get, envir=ICI)
  parametres<-lapply(PARAMACTUALISE2, function(x) {
	if(is.null(x$pix)) pix<-TRUE else pix<-x$pix
	return(c(list(niv=x$niv, pix=pix), lapply(x[setdiff(names(x), c("niv", "pix"))], forcage, niv=x$niv, cond=pix)) )
  })
  # temps<-system.time(mapply(do.call, what=fonctions, args= parametres))[2]
  # return(temps)
  mapply(do.call, what=fonctions, args= parametres)
}


farming<-function(...)
{
  if (OPTIONSIMU$printwhat>2) print("cultural practices")
  if (class(INTERVENTIONS$quand)=="Date") indice<-(INTERVENTIONS$quand == SIMUTIME) else indice<-(INTERVENTIONS$quand == convertit2())
  faisaction<-function(quoi, ou, comment) #une seule action = une seule parcelle ou cult_SdC
  {
      quelspix <- eval(parse(text= ou)) #conditions au niveau 1
      if (sum(quelspix)>0) {

          if (OPTIONSIMU$printwhat>1) {
              #find name of SdC_crop
              SdC_crop<-unlist(lapply(strsplit(ou, split="==", fixed=TRUE),"[[", 2))
              print(paste(quoi, "sur", SdC_crop, "(", sum(quelspix), "pixels)"))
          }
          #######
          ####print(unlist(lapply(MILIEU, function(x) lapply(x, function(y) sum(is.na(y))))))

            do.call(what=quoi,args=c(comment, pix=list(quelspix)))

          ######
          ####print(unlist(lapply(MILIEU, function(x) lapply(x, function(y) sum(is.na(y))))))


      }
      return()
  }
  # temps<-system.time(mapply(faisaction, quoi=INTERVENTIONS$quoi[indice], ou=INTERVENTIONS$ou[indice], comment=INTERVENTIONS$comment[indice]))[2]
  # return(temps)
  mapply(faisaction, quoi=INTERVENTIONS$quoi[indice], ou=INTERVENTIONS$ou[indice], comment=INTERVENTIONS$comment[indice])
}              
