# script de manipulation modeles PODYAM
###########################################
# REDIRECT ERRORS IF NEEDED - TEST JEROME #
###########################################

###########
# IMPORTS #
###########
library(devtools)
library(stringr)
library(lubridate)
library(data.table)
library(podyam)
options(stringsAsFactors = FALSE) 
graphics.off()

#############################
# DEFINE BASIC INFORMATIONS #
#############################
# Define some informations, like paths
RUN_ID        <- "basic_model_3y"
OUT_PATH      <- "../.tmp"
PREFIX_PATH = ""

###################
# DEFINE SCENARIO #
###################
SCENARIO<-list(
        utilitaries="progR_sources_utils.R",
        
        space="../landscapes/ExtraSimpleLandscape.Rdata",
        climate="../climates/simple_climate_basic_model.txt",
        plotting="progR_sources_dessine.R",

        extrafunctions="progR_sources_fonctionsuppl.R",
        dynamics="progR_sources_dyn.R",
        trophic="progR_sources_atta.R",
        agronomy="progR_sources_agronomie.R",
        handlers="progR_sources_handlers.R",
        landscape="progR_sources_landscapeHelpers.R",
        encoding="UTF-8",

        ### Those needed to reset the model - order matter
        # This have been tested,  they dont need the upper files to be sourced
        parameters="../parameters/simple_mosaicphoma_basic_model.R",
        init="progR_sources_init.R",
        dispersal="progR_sources_disp.R"
        ### END
)


#####################
# DEFINE REFERENCES #
#####################
REFERENCE_FILE = NULL # if given will ignore the manually given instructions
REFERENCE_MANUAL_DATA = list(
        dates = c("2016-01-01", "2019-01-01", 31),
        species_stages = list(
                "Lepto" = list("res1", "spore" )
        ),
        # see module_generateData for draw_functions
        collect_points = list(c(2,5), c(3,10),c(4,9), c(6,1), c(7,3), c(8,1), c(8,2), c(9,9), c(10,6), c(10,9)), # (x,y)
        #collect_points = list(location_type = 11, point_number = 10, draw_function = "simple_sample"),
        data_type = "POPULATIONS"
)


############################
# DEFINE FIRST RUN OPTIONS #
############################
DATE_DEBUT_SIMUL= "2016-01-01" # if NULL the 1st date of the ref is the 1st day of simulation or 2016 January 1st if no refs
how_many_times  = list(
        years  = 3,
        months = 0,
        days   = 0
)

#######################
# .~*~.COMPUTING .~*~.#
#######################
# Create model
#source("musclr/setup.R")
MODEL_ID = "myModel"
myModel = setup(SCENARIO)
DurationSimu = podyam:::how_long_is_simul(how_many_times, myModel)

myModel$setoptions(printwhat = 0)
# Run simulation
optimend = optim_podyam(myModel, fopt = "optim", evalFunc=adjust_model_least_square, ref_file_path='../references/reference_basic_model_3y_population_values.csv')

# Ploting results of optim (report needed)
csv <-read.csv2(file=paste0(OUT_PATH, "/", RUN_ID, "/", MODEL_ID, "_report.csv"), h=F)
csv <- csv[-c(1),]
df <- data.frame(csv$V1)
names(df)<- c("moindre_carres")
df$simutime <- seq(1, nrow(df), 1)
df$XToHalfSurvival <- csv$V3
df$sd <- csv$V4
for( i in names(df)){
         if(class(df[[i]]) == "character"){
                    df[[i]] = as.numeric(df[[i]])}
         if(class(df[[i]]) == "factor"){
                 df[[i]] =as.numeric(as.character(df[[i]]))}
        }
dfm <- melt(df, id="simutime")
ggplot(data=dfm, aes(x=simutime, y=value, colour=variable)) + geom_line() + labs(x="Itérations d'optim")
#unlink(paste0(OUT_PATH, "/", RUN_ID), recursive = TRUE)