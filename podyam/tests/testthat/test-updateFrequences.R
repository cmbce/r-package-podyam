# Test update frequences
test_that("BASIC MODEL RUN 2 + update freqs + 1 YEAR", {
    # Run the scenarii
    source('scenarii/tmp_updateFreqs.R', chdir = TRUE)
    # Load ref
    load('references/tmp_updatedFreqs.Rdata')
    # Compare
    expect_equal(myModel_ref$ICI$MILIEU$levapatch, myModel$ICI$MILIEU$levapatch)
    # Clean memory
    rm(list=ls())
    gc()
})
