######################
# Module description #
######################
#' @title A nice module about extracting some data
#' @name module_getData
#' @description Extract all data from a particular state
#' of the dynamics if it's in the references too.
#'
#' The main function is getData()
#' @usage NULL
######################
# END OF MODULE DESC #
######################


# @title format references for being used more easily
# @name format_references
# @description format references for being used more easily
#               it will split the columns location_col_row and species_stage
#               from the data.frame returned by module_generateData.
#
# @param model related to the references, created by setup(...)
# @param references raw references frame from module_generateData.
#
#' @import data.table
# @export
format_references <- function(model, references){
        # require(data.table)
        print("Formatting references...")

        # Split the columns and add ref_time
        not_before = references$posix_time[references$posix_time >= model$ICI$DATE_DEBUT_SIMUL]
        minDate = min(not_before, model$ICI$DATE_DEBUT_SIMUL)

        # Add ref_time and create location
        references[, ref_time := floor(posix_time/minDate + (posix_time-minDate)/ 86400)]
        references[, location := paste(col, "_", row, sep="")]
        # Forced formatting because R is bad at handling datatype
        references$ref_time<- as.numeric(references$ref_time)
        references$col     <- as.numeric(references$col)
        references$row     <- as.numeric(references$row)
        references$location<- as.character(references$location)
        references$species <- as.character(references$species)
        references$stage   <- as.character(references$stage)
        references$type    <- as.character(references$type)
        # Date to gregorian
        references$gregorian <- podyamDate_to_gregorianDate(references$posix_time)
        # Remove out of range locations
        maxcol <- max(model$ICI$ESPACE$lev1$col)
        maxrow <- max(model$ICI$ESPACE$lev1$row)
        tmp = references[col <= maxcol & row <= maxrow]
        if(tmp[,.N] < references[,.N]){
                message("Removed out of range locations")
        }
        references <- tmp
        # Clean memory
        rm(tmp)
        gc()
        # Ordering references
        setkey(references, ref_time, col, species)
        print("DONE.")
        return(references)
}

#' @title Look for matchs between references and Poydam datas for each timestep
#' @name getData
#' @description Return for each reference the value related for each timestep
#'              Currently it writes the results in a file called "modelName_match_refs.csv"
#'              see details
#'
#' @param this_env model environnment (myModel$ICI), created by setup(...)
#' @format
#'      !!! Currently due to "unpasdetemps()" not handling parameters this function
#'      is used with global var REFERENCES and the return is commented
#' <TAG2INCLUDE>
#' <TAG2INCLUDE>
#' @details OUTFILE name and location is created based on the information in
#' the scenario file: OUT_PATH/RUN_ID/MMODEL_ID_match_refs.csv.
#' If the path is relative it starts from the scenario.R
#'
#' @examples \dontrun{
#' /!\ See tests/testthat/scenarii/basic_model_noOptim.R along with
#' tests/testthat/parameters/simple_mosaicphoma_basic_model_noOptim.R for
#' optimization example
#'
#' # Do not forget to comment the final line in the scenario to avoid removal of created files
#' source("test/testthat/scenarii/basic_model_noOptim.R", chdir = TRUE)
#' # Add reference values to model
#' refs <- myModel$ICI$REFERENCES
#' refs$values <- read.csv2(file="tests/testthat/.tmp/basic_model_3y_nooptim/myModel_match_refs.csv", h=F)
#' refsvalues <- as.numeric(refs$values)
#' # Get some time
#' par(mfrow=c(1,3))
#' sub<-refs[ref_time==1 | ref_time==361 | ref_time==13]
#' plot(sub$col[sub$stage=="spore"], sub$row[sub$stage=="spore"], cex=sqrt(as.numeric(sub$values[sub$stage=="spore"])), col=sub$ref_time, xlab="col", ylab="row", main="for each location (row/col) the abundance of Lepto.spore at time 1 and time 361")
#' plot(myModel$ICI$EPIDEMIO['Lepto.cumu',], type="l", ylab="total biomass in landscape", xlab="simutime")
#' myModel$ICI$plotdynamics('Lepto.cumu') # same as previous
#' }
#' 
#' @import data.table
#' @export
getData <- function(this_env){
        references <- this_env$REFERENCES
        # require(data.table)
        sub <- references[ref_time==this_env$SIMUTIME]
        if(sub[,.N] < 1){
                return()
        }
        vector=numeric(sub[,.N])
        for(i in 1:sub[,.N]){
                # e.g. POPULATIONS$Lepto['1_2','spore']
                vector[i] <- get(sub$type[i], envir = this_env)[[sub$species[i]]][[sub$location[i], sub$stage[i]]]
                if(this_env$OPTIONSIMU$printwhat >= 5){
                       print(cbind(sub[i], value = vector[i]))
                }
        }
        # Since module call from "unpasdetemps" cant handle args we removed the
        # return for updating the global var previously
        # return(references)
        outfile = paste(this_env$OUT_PATH, "/", this_env$RUN_ID, "/", this_env$ID, "_match_refs.csv", sep="")
        write.table(vector, file=outfile, append=TRUE, sep=";", na="NA", row.names = FALSE, col.names = FALSE )
}